<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include  page="/header.jsp"></jsp:include>
<input id="isFormValid" type="hidden" value="${validationPpFormulaire }" disabled />
<input id="isRange" type="hidden" value="${isRange }" disabled />

<div class="container content-title">
	<h2 class="tab">Paramètres particuliers du restaurant </h2>
	<p class="common-text">Pour paramétrer les disponibilités du restaurant jour par jour</p>
</div>

<div id="params-premiere-connexion" class="container content">
	
	<c:if  test="${ not empty validationFormulaireParametres }">
		<div class="message-validation"><c:out value="${validationFormulaireParametres }"/></div>
	</c:if>
	<c:if  test="${ not empty erreurFormulaireParametres }">
		<div class="message-erreur"><c:out value="${erreurFormulaireParametres }"/></div>
	</c:if>
	<div class="flex-row-center-center infos-param-pp">
			<p class="common-text">
			La sélection de dates multiples est possible !
			<br/>
			Le paramétrage de ces dates sera prioritaire par rapport aux paramètres généraux	
			 </p>
			
		</div>
	<div class="d-flex justify-content-center align-content-center dpick" id="datepicker-pp"></div>
	
	<c:url var="post_url" value="/parametres-particuliers" />
	<form:form action="${post_url }"  method="POST" modelAttribute="parametreParticulierDto" id="form-pp">
		<form:errors cssClass="error-field" path="dates"/>
		
		<div class="infos-param-pp">
			<p class="flex-row-center-center common-text"><span id="red-square"></span> : Jours déjà paramétrés</p>
		</div>
		<div class="container tab-resas" id="infosParametres">
			<div class="table-block">
				<h3>Paramètres du jour sélectionné</h3>
				<div class="outter-form">
					<table id="parametresParticulier" class="table resa-table table-striped">
						<tr>
							<th>Date</th>
							<th>Ouvert le midi ?</th>
							<th>Ouvert le soir ?</th>
							<th>Créneau 1 midi</th>
							<th>Créneau 2 midi</th>
							<th>Créneau 3 midi</th>
							<th>Créneau 1 soir</th>
							<th>Créneau 2 soir</th>
							<th>Créneau 3 soir</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
		
		<form:hidden id="tabDates" path="dates" value=""/>
		<form:hidden id="isMulti" path="multi" value=""/>

		<div class="inner-ouverture flex-col-center">
			<h3>Ouvert le :</h3>
			
				<div class="flex-row-center">
					<div class="inner-ouverture-check flex-col-center">
						<form:checkbox id="ouvert-midi" path="ouvertMidiP" />
						<form:label path="ouvertMidiP">Midi</form:label>
					</div>
					<div class="inner-ouverture-check flex-col-center">
						<form:checkbox id="ouvert-soir" path="ouvertSoirP" />
						<form:label path="ouvertSoirP">Soir</form:label>
					</div>
				</div>
				<h3>Services du midi</h3>
				<div class="params-item creneaux">
					<div class="inner-creneau flex-row-center-center">
						<c:forEach begin="1" end="3" step="1" var="loopMidi">
							<div>
							<div class="creneau-debut flex-col-center">
								<form:errors cssClass="error-field" path="creneau${loopMidi}MidiDebut"/>
								<form:label path="creneau${loopMidi}MidiDebut">Début du service n°${loopMidi} du midi <c:if test="${loopMidi == 1}" >(Obligatoire)</c:if><c:if test="${loopMidi != 1}" >(Optionnel)</c:if></form:label>
								<form:select path="creneau${loopMidi}MidiDebut">
									<form:option value=""> --Sélectionnez--</form:option>
									<c:forEach begin="08" end="16" step="1" var="loopH">
										<c:forEach begin="00" end="59" step="15" var="loopM">
											<c:choose>
												<c:when test="${loopH < 10 && loopM < 10}">
													<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
												</c:when>
												<c:when test="${loopH < 10 && loopM >= 10}">
													<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
												</c:when>
												<c:when test="${loopH >= 10 && loopM < 10}">
													<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
												</c:when>
												<c:when test="${loopH >= 10 && loopM >= 10}">
										    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
										    	</c:when>
									    	</c:choose>
										</c:forEach>
									</c:forEach>
								</form:select>
								
							</div>
							<div class="creneau-fin flex-col-center">
								<form:label path="creneau${loopMidi}MidiFin">Fin  du service n°${loopMidi} du midi <c:if test="${loopMidi == 1}" >(Obligatoire)</c:if><c:if test="${loopMidi != 1}" >(Optionnel)</c:if></form:label>
								<form:select path="creneau${loopMidi}MidiFin">
									<form:option value=""> --Sélectionnez--</form:option>
									<c:forEach begin="08" end="16" step="1" var="loopH">
										<c:forEach begin="00" end="59" step="15" var="loopM">
											<c:choose>
												<c:when test="${loopH < 10 && loopM < 10}">
													<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
												</c:when>
												<c:when test="${loopH < 10 && loopM > 10}">
													<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
												</c:when>
												<c:when test="${loopH >= 10 && loopM < 10}">
													<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
												</c:when>
												<c:when test="${loopH >= 10 && loopM > 10}">
										    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
										    	</c:when>
									    	</c:choose>
										</c:forEach>
									</c:forEach>
								</form:select>
								<form:errors cssClass="error-field" path="creneau${loopMidi}MidiFin"/>
							</div>
							<div class="nb-couverts flex-col-center">
								<form:label path="creneau${loopMidi}MidiNbResas">Nombre de réservations maximum pour ce service</form:label>
								<form:input type="number" path="creneau${loopMidi}MidiNbResas" />
								<form:errors cssClass="error-field" path="creneau${loopMidi}MidiNbResas"/>
							</div>
						</div>
						</c:forEach>
					</div>
				</div>
			
				<h3>Services du soir</h3>
				<div class="params-item creneaux">
			<div class="inner-creneau flex-row-center-center">
				<c:forEach begin="1" end="3" step="1" var="loopSoir">
				<div>
					<div class="creneau-debut flex-col-center">
						<form:label path="creneau${loopSoir}SoirDebut">Début du service n°${loopSoir} du soir <c:if test="${loopSoir == 1}" >(Obligatoire)</c:if><c:if test="${loopSoir != 1}" >(Otpionnel)</c:if></form:label>
						<form:select path="creneau${loopSoir}SoirDebut">
							<form:option value=""> --Sélectionnez--</form:option>
							<c:forEach begin="16" end="23" step="1" var="loopH">
								<c:forEach begin="00" end="59" step="15" var="loopM">
									<c:choose>
										<c:when test="${loopH < 10 && loopM < 10}">
											<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH < 10 && loopM > 10}">
											<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM < 10}">
											<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM > 10}">
								    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
								    	</c:when>
							    	</c:choose>
								</c:forEach>
							</c:forEach>
						</form:select>
						<form:errors cssClass="error-field" path="creneau${loopSoir}SoirDebut"/>
					</div>
					<div class="creneau-fin flex-col-center">
						<form:label path="creneau${loopSoir}SoirFin">Fin  du service n°${loopSoir} du soir <c:if test="${loopSoir == 1}" >(Obligatoire)</c:if><c:if test="${loopSoir != 1}" >(Optionnel)</c:if></form:label>
						<form:select path="creneau${loopSoir}SoirFin">
							<form:option value=""> --Sélectionnez--</form:option>
							<c:forEach begin="16" end="23" step="1" var="loopH">
								<c:forEach begin="00" end="59" step="15" var="loopM">
									<c:choose>
										<c:when test="${loopH < 10 && loopM < 10}">
											<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH < 10 && loopM > 10}">
											<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM < 10}">
											<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM > 10}">
								    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
								    	</c:when>
							    	</c:choose>
								</c:forEach>
							</c:forEach>
						</form:select>
						<form:errors cssClass="error-field" path="creneau${loopSoir}SoirFin"/>
					</div>
					<div class="nb-couverts flex-col-center">
						<form:label path="creneau${loopSoir}SoirNbResas">Nombre de réservations maximum pour ce service</form:label>
						<form:input type="number" path="creneau${loopSoir}SoirNbResas" />
						<form:errors cssClass="error-field" path="creneau${loopSoir}SoirNbResas"/>
					</div>
				</div>
			</c:forEach>
			</div>
		</div>
		</div>
		<div class="form-button-block mt40">
			<form:button class="form-button" type="submit">Valider</form:button>
		</div>
	</form:form>
</div>

<jsp:include  page="/footer.jsp"></jsp:include>

