<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bienvenue</title>
    <link href="<c:url value='/resources/css/bootstrap.min.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/common.css' />" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value='/resources/css/tavo-calendar.css' />" />
    <link rel="stylesheet" href="<c:url value='/resources/css/jqueryui-1.12.1.css' />" />
    <link rel="stylesheet" href="<c:url value='/resources/css/jquery-ui.multidatespicker.css' />" />
    <c:if test="${not fn:endsWith(pageContext.request.requestURI, '/parametres-particuliers')}">
   		<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/jquery.datepick.css' />">
	</c:if>
    <c:set var="userToken" value="${userToken }" />
	<link id="contextPathHolder" data-contextPath="${contextPath}"/>
</head>
<body>

	<div class="navigation container-fluid">
		<nav class="navbar navbar-expand-lg">
 			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarprincipal" aria-controls="#navbarprincipal" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon">
			    	<svg viewBox="0 0 100 80" width="30" height="30">
					  <rect class="mb-green" width="100" height="20" rx="10"></rect>
					  <rect class="mb-green" y="30" width="100" height="20" rx="10"></rect>
					  <rect class="mb-green" y="60" width="100" height="20" rx="10"></rect>
					</svg>
			    </span>
		  	</button>
			<div class="collapse navbar-collapse" id="navbarprincipal">
				<ul id="nav" class="nav navbar navbar-nav flex-row-start-center">
					<li class="nav-item"><a href="<c:url value='/home' />" class="nav-link">Accueil</a></li>
					<li class="nav-item"><a href="<c:url value='/tableau-bord' />" class="nav-link">Résumé</a></li>
					<li>
						<div class="dropdown">
						  <a href="<c:url value='/set-params' />" class="dropbtn nav-item nav-link">Paramètres</a>
						  <div class="dropdown-content">
						    <a class="dropdown-item nav-link" href="<c:url value='/set-params' />">Paramètres généraux</a>
						    <a class="dropdown-item nav-link" href="<c:url value='/parametres-particuliers' />">Paramètres particuliers</a>
						  </div>
						</div>
					</li>
					<li>
						<div class="dropdown">
						  <a href="<c:url value='/reservations-consulter' />" class="dropbtn nav-item nav-link">Réservations</a>
						  <div class="dropdown-content">
						    <a class="dropdown-item nav-link" href="<c:url value='/reservations-consulter' />">Consulter</a>
						    <a class="dropdown-item nav-link" href="<c:url value='/reservation' />">Ajouter</a>
						  </div>
						</div>
					</li>
					<li class="nav-item"><a href="<c:url value='/rapports' />" class="nav-link">Rapports</a></li>
				</ul>
			
				<a href="<c:url value='/home' />" ><img  id="logo" src="<c:url value='/resources/img/logo_vert.svg' />"></a>
				<span class="nav-gest">Bienvenue ${pageContext.request.userPrincipal.name} | <a href="<c:url value='/login?logout' />" >Déconnexion</a></span>
			</div>
		</nav>
	</div>
	
	<div class="container-fluid top-container">
		<h1><c:out value="${nomPage }" /></h1>
	</div>
	<div class="container">
	    <c:if test="${pageContext.request.userPrincipal.name != null}">
	        <form id="logoutForm" method="POST" action="<c:url value='/logout' />">
	            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	        </form>       
	    </c:if>
  	</div>
  	
	