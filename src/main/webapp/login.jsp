<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Se connecter à l'application</title>

      <link href="<c:url value='/resources/css/bootstrap.min.css' />" rel="stylesheet">
      <link href="<c:url value='/resources/css/common.css' />" rel="stylesheet">
  </head>

  <body>

    <div class="container container-login">
      
      <form method="POST" action="<c:url value='/login' />" class="form-signin">
       	<img  id="logobb" src="<c:url value='/resources/img/logo_bb_carre.svg' />">
        <h2 class="form-heading">Connexion</h2>
        <div class="form-group ${error != null ? 'has-error' : ''}">
            <span>${message}</span>
            <input name="username" type="text" class="form-control" placeholder="Utilisateur"
                   autofocus="true"/>
            <input name="password" type="password" class="form-control" placeholder="Mot de passe"/>
            <span>${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            <button class="form-button login-bt btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
        </div>
      </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
  </body>
</html>
