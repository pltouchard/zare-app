<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<jsp:include  page="/header.jsp"></jsp:include>
	<div class="container content-title">
		<h2 class="tab">Nouvelle réservation</h2>
	</div>

	<div class="container d-flex justify-content-center align-content-center flex-column mt40 mb40 content">
		
		<c:if  test="${ not empty messageAjoutFormulaire }">
			<div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
		</c:if>
		<c:if  test="${ not empty messageErreurFormulaire }">
			<div class="message-erreur"><c:out value="${messageErreurFormulaire }"/></div>
		</c:if>
		
		<c:choose>
		<c:when test="${not empty  errors}">
		    <div class="error">
		    <c:forEach items="${errors}" var="err">
		        ${err.defaultMessage}
		        <br/>
		    </c:forEach>
		    </div>
		</c:when>
		</c:choose>
		
		<div class="d-flex justify-content-center align-content-center dpick" id="datepicker"></div>
		<form:form id="formResa"  cssClass="text-center mt40 form-resa" method="POST" action="reservation-ajout" modelAttribute="reservationDto">
			<form:hidden id="dateAReserver" path="dateResa" value=""/>
			<form:hidden path="idResa" />
			
			<div class="inner-form">
				<div class="inner-form-block inner-form-1">
					<div>
						<div class="">
							<form:input path="nomResa" placeholder="Nom de la réservation" required="required"/>
							<form:errors cssClass="error-field" path="nomResa"/>
						</div>
						<div class="check flex-row-center">
							<form:checkbox path="habitue"/>
							<form:label path="habitue">Client habitué ?</form:label>
						</div>
					</div>
					
					<div class="">
						<form:input path="telephoneResa" placeholder="Téléphone de la réservation" required="required"/>
						<form:errors cssClass="error-field" path="telephoneResa"/>
					</div>
					<div class="">
						<form:input path="mailResa" placeholder="Email de la réservation" required="required"/>
						<form:errors cssClass="error-field" path="mailResa"/>
					</div>
				</div>
				<div class="inner-form-block inner-form-2">
					<div class="">
						<form:input path="nbCouvertsResa" type="number" min="0" placeholder="Nombre de couverts" required="required"/>
						<form:errors cssClass="error-field" path="nbCouvertsResa"/>
					</div>
					
					<div class="">
						<form:select path="creneauResaId" required="required">
							<form:option value="0"> -- Choisir un créneau--</form:option>
							<c:forEach var="creneau" items="${tousLesCreneaux}">
									<c:choose>
										<c:when test="${idCreneau == creneau.idCreneau }">
										   <form:option value="${creneau.idCreneau}" selected="selected"><c:out value="${creneau.creneauDebut} - ${creneau.creneauFin}" /></form:option>
										</c:when>
										
										<c:otherwise>
										   <form:option value="${creneau.idCreneau}"><c:out value="${creneau.creneauDebut} - ${creneau.creneauFin}"/></form:option>
										</c:otherwise>
									</c:choose>
						    </c:forEach>
						</form:select>
						<form:errors cssClass="error-field" path="creneauResaId"/>
					</div>
					
					<div class="">
						<form:textarea path="notesResa" placeholder="Notes sur la réservation"></form:textarea>
						<form:errors path="notesResa"></form:errors>
						<div class="text-left flex-row-center check">
							<form:checkbox path="notesResaImportant"/>
							<form:label path="notesResaImportant">Notes importantes ? (afficher sur le tableau de Bord ?)</form:label>
						</div>
					</div>
				</div>
			</div>
			<form:hidden path="showResa" value="true"/>
			<button class="form-button" type="submit"><c:out value="${(empty reservationFormulaire) ? 'Valider' : 'Modifier' }" /></button>
			<c:if test="${reservationDto.idResa > 0}">
				<a id="annulerModif" href="<c:url value='/reservations-consulter' />"  >Annuler</a>
			</c:if>
		</form:form>
	</div>

<jsp:include  page="/footer.jsp"></jsp:include>