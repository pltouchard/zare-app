<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include  page="/header.jsp"></jsp:include>
<div class="container content-title">
	<h2 class="tab">Consulter les réservations</h2>
</div>

<div class="container d-flex justify-content-center align-content-center flex-column mt40 mb40 content">
	<c:if  test="${ not empty messageAjoutFormulaire }">
		<div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	<c:if  test="${ not empty messageErreurFormulaire }">
		<div class="message-erreur"><c:out value="${messageErreurFormulaire }"/></div>
	</c:if>
	
	
	<div class="dpick d-flex justify-content-center align-content-center" id="consulterdatepicker"></div>
	
	<div class="container d-flex justify-content-center mt40">
		<p class="date-resa">Réservations du <span id="dateAffichageReservations"></span> </p>
	</div>

	
	<div class="container tab-resas">
		<p id="aucuneReservation" class=""></p>
			
		<div class="info-pictos container d-flex mt40 flex-col-center">
			<p class="picto"><span><img alt="picto" src="/resources/img/couronne.svg"></span> = client habitué</p>
			<p class="picto"><span><img alt="picto" src="/resources/img/important.svg"></span> = note importante</p>
		</div>
		
		<div class="table-block">
			<h3 class="mt40 text-center">Service du midi</h3>
			<div class="outter-form">
				<table id="reservations-midi" class="table resa-table">
					<thead>
						<tr>
							<th class="text-center">Service</th>
							<th class="text-center">Nom</th>
							<th class="text-center">Téléphone</th>
							<th class="text-center">Mail</th>
							<th class="text-center">Couverts</th>
							<th class="text-center">Notes</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody id="tbody-reservations-midi">
					</tbody>
				</table>
			</div>
			</div>
		<div class="table-block">
			<h3 class="mt40 text-center">Service du soir</h3>
			<div class="outter-form">
				<table id="reservations-soir" class="table resa-table">
					<thead>
						<tr>
							<th class="text-center">Service</th>
							<th class="text-center">Nom</th>
							<th class="text-center">Téléphone</th>
							<th class="text-center">Mail</th>
							<th class="text-center">Couverts</th>
							<th class="text-center">Notes</th>
							<th class="text-center">Actions</th>
						</tr>
					</thead>
					<tbody id="tbody-reservations-soir" >
					</tbody>
				</table>
			</div>
		</div>
		<div id="dialog-confirm" title="Supprimer la réservation ?">
		  <p>La réservation sera définitivement supprimée. êtes-vous sûr ?</p>
		</div>
	</div>

</div>
<jsp:include  page="/footer.jsp"></jsp:include>

