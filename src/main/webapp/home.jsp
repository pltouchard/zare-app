<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include  page="/header.jsp"></jsp:include>


<div class="panel-body" id="result">
</div>

<div class="container d-flex justify-content-center mt40">
		<div class="d-flex justify-content-center align-content-center dpick" id="homedatepicker"></div>
</div>

<div class="container">
  	<h2 class="text-center mt40">Réservations du <span id="dateResaConsultation"></span></h2>
</div>

<div class="container-fluid bg-light mt40">
	<div id="container-midi" class="container">
		<h3 class="m20">Réservations du midi</h3>
	</div>
	<div class="container d-flex justify-content-between inner-resume">
		<div class="">
			<h4 class="text-center" >Nombre de couverts réservés :</h4>
			<p id="couvertsJourMidi" class="text-center"></p>
		</div>
		
		<div class="">
			<h4 class="text-center" >Nombre de tables réservés :</h4>
			<p id="resasTablesMidi" class="text-center"></p>
			<div id="tablesResasMidi" class="text-center">
				
			</div>
		</div>
		
		<div class="">
			<h4 class="text-center" >Nombre de couverts réservés par service :</h4>
			<p class="text-center">Premier Service (<span id="hCreneau1Midi"></span>) : <span id="resaCreneau1Midi"></span></p>
			<p class="text-center hidden-creneau-midi2">Deuxième Service (<span id="hCreneau2Midi"></span>)  : <span id="resaCreneau2Midi"></span></p>
			<p class="text-center hidden-creneau-midi3">Troisième Service (<span id="hCreneau3Midi"></span>)  : <span id="resaCreneau3Midi"></span></p>
		</div>
	</div>
	<div class="text-center">
		<h3>Informations importantes du service : </h3>
		<table id="notes-midi" class="table table-striped">
			<tr>
				<th>Nom réservation</th>
				<th>Note sur la réservation</th>
			</tr>
		</table>
		
		<ul id="liste-habitues">
			
		</ul>
	</div>
</div>
<div class="container-fluid bg-light mt40">
	<div id="container-soir" class="container">
		<h3 class="m20">Réservations du soir</h3>
	</div>
	<div class="container d-flex justify-content-between inner-resume"> 
		<div class="">
			<h4 class="text-center">Nombre de couverts réservés :</h4>
			<p id="couvertsJourSoir" class="text-center"></p>
		</div>
		
		<div class="">
			<h4 class="text-center" >Nombre de tables réservées :</h4>
			<p id="resasTablesSoir" class="text-center"></p>
			<div id="tablesResasSoir" class="text-center">
				
			</div>
		</div>
		
		<div class="">
			<h4 class="text-center" >Nombre de couverts réservés par service :</h4>
			<p class="text-center">Premier Service (<span id="hCreneau1Soir"></span>)  : <span id="resaCreneau1Soir"></span></p>
			<p class="text-center hidden-creneau-soir2">Deuxième Service (<span id="hCreneau2Soir"></span>) : <span id="resaCreneau2Soir"></span></p>
			<p class="text-center hidden-creneau-soir3">Troisième Service (<span id="hCreneau3Soir"></span>) : <span id="resaCreneau3Soir"></span></p>
		</div>
	</div>
	<div class="text-center">
		<h3>Informations importantes du service : </h3>
		<table id="notes-soir" class="table table-striped">
			<tr>
				<th>Nom réservation</th>
				<th>Note sur la réservation</th>
			</tr>
		</table>
		
		<ul id="liste-habitues">
			
		</ul>
	</div>
</div>

<!-- <c:if test="${afficherPremiersParams == 'afficherPremiersParams'}">
	<jsp:include page="${contextPath}/parametres-premiere.jsp"></jsp:include>
</c:if>  -->
	
<jsp:include  page="/footer.jsp"></jsp:include>