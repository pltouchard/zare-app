<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false"%>
<html>
<head>
    <title>Erreur</title>
</head>
<body>
    <h1>${errorMsg}</h1>
    <p>Une erreur est survenue !</p>
    </br>
    <p>Merci de vous reconnecter à l'application. Si le problème persiste, n'hésitez pas à contacter votre contact technique</p>
    <c:choose>
    <c:when test="${errorCode != 400 ||  errorCode != 404}">
    	<a href="<c:url value="/login?connection" />" >Retourner à l'accueil</a>
    </c:when>
    <c:otherwise>
    	<a href="<c:url value="/" />" >Retourner à l'accueil</a>
    </c:otherwise>
    </c:choose>
</body>
</html>