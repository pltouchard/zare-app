<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include  page="/header.jsp"></jsp:include>

<div class="container content-title">
	<h2 class="tab">Paramètres généraux du restaurant </h2>
	<p>Pour paramétrer les disponibilités du restaurant de façon générale</p>
</div>

<c:if  test="${ not empty validationFormulaireParametres }">
	<div class="message-validation"><c:out value="${validationFormulaireParametres }"/></div>
</c:if>
<c:if  test="${ not empty erreurFormulaireParametres }">
	<div class="message-erreur"><c:out value="${erreurFormulaireParametres }"/></div>
</c:if>


<div id="params-premiere-connexion" class="container content">
	
	<h2><c:out value="${erreurMsg }" /></h2>
	<c:url var="post_url" value="/set-params" />
	<form:form action="${post_url }"  method="POST" modelAttribute="parametresDto">
		
		<h3>Services du midi</h3>
		<div class="params-item creneaux flex-row-center">
			<div class="inner-creneau flex-row-center-center">
				<c:forEach begin="1" end="3" step="1" var="loopMidi">
					<div class="parent-creneau">
					<div class="creneau-debut flex-col-center">
						<form:errors cssClass="error-field" path="creneau${loopMidi}MidiDebut"/>
						<form:label path="creneau${loopMidi}MidiDebut">Début du service n°${loopMidi} du midi <c:if test="${loopMidi == 1}" >(Obligatoire)</c:if><c:if test="${loopMidi != 1}" >(Optionnel)</c:if></form:label>
						<form:select path="creneau${loopMidi}MidiDebut">
							<form:option value=""> --Sélectionnez--</form:option>
							<c:forEach begin="08" end="16" step="1" var="loopH">
								<c:forEach begin="00" end="59" step="15" var="loopM">
									<c:choose>
										<c:when test="${loopH < 10 && loopM < 10}">
											<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH < 10 && loopM > 10}">
											<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM < 10}">
											<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM > 10}">
								    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
								    	</c:when>
							    	</c:choose>
								</c:forEach>
							</c:forEach>
						</form:select>
						
					</div>
					<div class="creneau-fin flex-col-center">
						<form:label path="creneau${loopMidi}MidiFin">Fin  du service n°${loopMidi} du midi <c:if test="${loopMidi == 1}" >(Obligatoire)</c:if><c:if test="${loopMidi != 1}" >(Optionnel)</c:if></form:label>
						<form:select path="creneau${loopMidi}MidiFin">
							<form:option value=""> --Sélectionnez--</form:option>
							<c:forEach begin="08" end="16" step="1" var="loopH">
								<c:forEach begin="00" end="59" step="15" var="loopM">
									<c:choose>
										<c:when test="${loopH < 10 && loopM < 10}">
											<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH < 10 && loopM > 10}">
											<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM < 10}">
											<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM > 10}">
								    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
								    	</c:when>
							    	</c:choose>
								</c:forEach>
							</c:forEach>
						</form:select>
						<form:errors cssClass="error-field" path="creneau${loopMidi}MidiFin"/>
					</div>
					<div class="nb-couverts flex-col-center">
						<form:label path="creneau${loopMidi}MidiNbResas">Nombre de réservations maximum pour ce service</form:label>
						<form:input type="number" path="creneau${loopMidi}MidiNbResas" />
						<form:errors cssClass="error-field" path="creneau${loopMidi}MidiNbResas"/>
					</div>
				</div>
				</c:forEach>
			</div>
		</div>
	
		<h3>Services du soir</h3>
		<div class="params-item creneaux flex-row-center">
			<div class="inner-creneau flex-row-center-center">
				<c:forEach begin="1" end="3" step="1" var="loopSoir">
				<div class="parent-creneau">
					<div class="creneau-debut flex-col-center">
						<form:label path="creneau${loopSoir}SoirDebut">Début du service n°${loopSoir} du soir <c:if test="${loopSoir == 1}" >(Obligatoire)</c:if><c:if test="${loopSoir != 1}" >(Optionnel)</c:if></form:label>
						<form:select path="creneau${loopSoir}SoirDebut">
							<form:option value=""> --Sélectionnez--</form:option>
							<c:forEach begin="16" end="23" step="1" var="loopH">
								<c:forEach begin="00" end="59" step="15" var="loopM">
									<c:choose>
										<c:when test="${loopH < 10 && loopM < 10}">
											<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH < 10 && loopM > 10}">
											<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM < 10}">
											<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM > 10}">
								    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
								    	</c:when>
							    	</c:choose>
								</c:forEach>
							</c:forEach>
						</form:select>
						<form:errors cssClass="error-field" path="creneau${loopSoir}SoirDebut"/>
					</div>
					<div class="creneau-fin flex-col-center">
						<form:label path="creneau${loopSoir}SoirFin">Fin  du service n°${loopSoir} du soir <c:if test="${loopSoir == 1}" >(Obligatoire)</c:if><c:if test="${loopSoir != 1}" >(Optionnel)</c:if></form:label>
						<form:select path="creneau${loopSoir}SoirFin">
							<form:option value=""> --Sélectionnez--</form:option>
							<c:forEach begin="16" end="23" step="1" var="loopH">
								<c:forEach begin="00" end="59" step="15" var="loopM">
									<c:choose>
										<c:when test="${loopH < 10 && loopM < 10}">
											<form:option value="0${loopH}:0${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH < 10 && loopM > 10}">
											<form:option value="0${loopH}:${loopM}:00"><c:out value="0${loopH}"/>:<c:out value="${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM < 10}">
											<form:option value="${loopH}:0${loopM}:00"><c:out value="${loopH}"/>:<c:out value="0${loopM}"/></form:option>
										</c:when>
										<c:when test="${loopH >= 10 && loopM > 10}">
								    		<form:option value="${loopH}:${loopM}:00"><c:out value="${loopH}"/>:<c:out value="${loopM}"/></form:option>
								    	</c:when>
							    	</c:choose>
								</c:forEach>
							</c:forEach>
						</form:select>
						<form:errors cssClass="error-field" path="creneau${loopSoir}SoirFin"/>
					</div>
					<div class="nb-couverts flex-col-center">
						<form:label path="creneau${loopSoir}SoirNbResas">Nombre de réservations maximum pour ce service</form:label>
						<form:input type="number" path="creneau${loopSoir}SoirNbResas" />
						<form:errors cssClass="error-field" path="creneau${loopSoir}SoirNbResas"/>
					</div>
				</div>
			</c:forEach>
			</div>
		</div>
	
		<h3>Ouvertures hebdomadaires</h3>
		<div class="ouverture-hebdo">
			<div class="semaine-param">
				<div class="ouverture-block">
					<h4>Lundi</h4>
					<div class="inner-ouverture flex-row-center">
						
						<div class="inner-ouverture-check  flex-row-center">
							<form:checkbox id="lundi-midi" path="ouvertLundiMidiG" />
							<form:label path="ouvertLundiMidiG">Midi</form:label>
						</div>
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="lundi-soir" path="ouvertLundiSoirG" />
							<form:label path="ouvertLundiSoirG">Soir</form:label>
						</div>
						
					</div>
				</div>
				<div class="ouverture-block">
					<h4>Mardi</h4>
					<div class="inner-ouverture flex-row-center">
						
						<div class="inner-ouverture-check  flex-row-center">
							<form:checkbox id="mardi-midi" path="ouvertMardiMidiG" />
							<form:label path="ouvertMardiMidiG">Midi</form:label>
						</div>
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="mardi-soir" path="ouvertMardiSoirG" />
							<form:label path="ouvertMardiSoirG">Soir</form:label>
						</div>
						
					</div>
				</div>
				<div class="ouverture-block">
					<h4>Mercredi</h4>
					<div class="inner-ouverture flex-row-center">
						
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="mercredi-midi" path="ouvertMercrediMidiG" />
							<form:label path="ouvertMercrediMidiG">Midi</form:label>
						</div>
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="mercredi-soir" path="ouvertMercrediSoirG" />
							<form:label path="ouvertMercrediSoirG">Soir</form:label>
						</div>
						
					</div>
				</div>
				<div class="ouverture-block">
				<h4>Jeudi</h4>
				<div class="inner-ouverture flex-row-center">
					
					<div class="inner-ouverture-check flex-row-center">
						<form:checkbox id="jeudi-midi" path="ouvertJeudiMidiG" />
						<form:label path="ouvertJeudiMidiG">Midi</form:label>
					</div>
					<div class="inner-ouverture-check flex-row-center">
						<form:checkbox id="jeudi-soir" path="ouvertJeudiSoirG" />
						<form:label path="ouvertJeudiSoirG">Soir</form:label>
					</div>
					
				</div>
			</div>
			</div>
			<div class="we-params">
				<div class="ouverture-block">
					<h4>Vendredi</h4>
					<div class="inner-ouverture flex-row-center">
						
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="vendredi-midi" path="ouvertVendrediMidiG" />
							<form:label path="ouvertVendrediMidiG">Midi</form:label>
						</div>
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="vendredi-soir" path="ouvertVendrediSoirG" />
							<form:label path="ouvertVendrediSoirG">Soir</form:label>
						</div>
						
					</div>
				</div>
				<div class="ouverture-block">
					<h4>Samedi</h4>
					<div class="inner-ouverture flex-row-center">
						
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="samedi-midi" path="ouvertSamediMidiG" />
							<form:label path="ouvertSamediMidiG">Midi</form:label>
						</div>
						<div class="inner-ouverture-check flex-row-center">
							<form:checkbox id="samedi-soir" path="ouvertSamediSoirG" />
							<form:label path="ouvertSamediSoirG">Soir</form:label>
						</div>
						
					</div>
				</div>
				<div class="ouverture-block">
				<h4>Dimanche</h4>
				<div class="inner-ouverture flex-row-center">
					
					<div class="inner-ouverture-check flex-row-center">
						<form:checkbox id="dimanche-midi" path="ouvertDimancheMidiG" />
						<form:label path="ouvertDimancheMidiG">Midi</form:label>
					</div>
					<div class="inner-ouverture-check flex-row-center">
						<form:checkbox id="dimanche-soir" path="ouvertDimancheSoirG" />
						<form:label path="ouvertDimancheSoirG">Soir</form:label>
					</div>
					
				</div>
			</div>
			</div>
		</div>
		
		<div class="form-button-block mt40">
			<form:button class="form-button" id="validFormResa"  type="submit">Valider</form:button>
		</div>
	</form:form>
</div>

<jsp:include  page="/footer.jsp"></jsp:include>

