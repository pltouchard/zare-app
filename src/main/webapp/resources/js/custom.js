$( document ).ready(function() {
	
	var dateToday = new Date();
	dateToday.setHours(0,0,0,0);
	  
	  
	//get path
	let CONTEXT_PATH = $('#contextPathHolder').attr('data-contextPath');
	let pathname = window.location.pathname;
//	var urlToContact = "http://localhost:8080/api-zare";
	var urlToContact = "http://localhost:9094";
	var tokenUser = $("#tokenJwt").val();
	var context = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
	
	//js header général
	document.querySelector('#nav > li:first-child').classList.remove('active');
	switch(pathname){
		case "/reservation" :
			document.querySelector('#nav > li:first-child').classList.add("active");
			break;
		case "/set-params":
		case "/parametres-pariculiers":
			document.querySelector('#nav > li:nth-child(3)').classList.add("active");
			break;
		case "/reservations-consulter":
			document.querySelector('#nav > li:nth-child(4)').classList.add("active");
			break;
		case "/rapports":
			document.querySelector('#nav > li:nth-child(5)').classList.add("active");
			break;
		case "/tableau-bord":
			document.querySelector('#nav > li:nth-child(2)').classList.add("active");
	}
	console.log(pathname);
	
	if(pathname == CONTEXT_PATH+"/tableau-bord"){
		
		$( "#homedatepicker" ).datepicker({
	    	dateFormat: "yy-mm-dd",
	    	numberOfMonths: 2,
	    	changeMonth: true,
	  		changeYear: true,
	    });
		
		 var creneaux = {};
		 
		// get date and all data related on page load
		var currentDate = $('#homedatepicker').val()
		checkParametresType(currentDate);
		
		// get date and all data related on click on a date on datepicker
		$("#homedatepicker").on("change",function(){
			creneaux = {};
	        var selected = $(this).val();
	        checkParametresType(selected);
	    });
		
		
		 //check the type of the parameters for a specific date
		 function checkParametresType(date){
			 getParametreType=$.ajax({
				 url: urlToContact + "/api/parametres-particuliers?dateParamPDate1=" + date,
				 headers: {
					 'Authorization': 'Bearer ' + tokenUser,
				 },
				 data: {
	                 method: 'GET',
	                 dataType: 'json', 
	             },
	             success: function( result ) {
	                 if(result == null || result == undefined){
	                	 getcreneauxForReservations(null, date);
	                	 
	                 }else{
	                	 getcreneauxForReservations(result, date);
	                 }
	             }
			 });
		 }
		 
		 //get crénaux depending of the type of the parameters set for a specific day
		 function getcreneauxForReservations(typeParam, selected){
			 var urlToGet;
			 if(typeParam == 'undefined' || typeParam == null){
				 urlToGet = urlToContact + "/api/parametres";
			 }else{
				 urlToGet = urlToContact + "/api/parametres-particuliers?dateParamPDate1=" + selected;
			 }

			// get all créneaux from API
			 getParamsCreneaux=$.ajax({
	             url: urlToGet,
	             headers: {
			    		'Authorization': 'Bearer ' + tokenUser,
	             },
	             data: {
	                 method: 'GET', 
	                 dataType: 'json', 
	             },
	             success: function( result ) {
	            	 ajaxGetResults(selected);
	             }
	         });

	         //get all créneaux
			 getParamsCreneaux.done(function(result){
				 if(result[0] != undefined){
					 result = result[0];
				 }else{
					 result = result;
				 }
				 
				 //get all créneaux available for midday
				 if(result.creneau1Midi.creneauDebut != "00:01:00"){
					 creneaux.midi1 = result.creneau1Midi;
					 if(result.creneau2Midi != null){
						 creneaux.midi2 = result.creneau2Midi;
					 }
					 if(result.creneau3Midi != null){
						 creneaux.midi3 = result.creneau3Midi;
					 }
				 }
				 
				 if(result.creneau1Soir.creneauDebut != "00:03:00"){
					 //get all créneaux available for the evening
					 creneaux.soir1 = result.creneau1Soir;
					 if(result.creneau2Soir != null){
						 creneaux.soir2 = result.creneau2Soir;
					 }
					 if(result.creneau3Soir != null){
						 creneaux.soir3 = result.creneau3Soir;
					 }
		 		}
				 
	         });
		}
		 

		//get all reservations
		function ajaxGetResults(selectedDate){
			$('#tablesResasMidi').children().remove();
			$('#tablesResasSoir').children().remove();
			$('#notes-midi > tbody > tr:not(:first-child)').remove();
			$('#notes-soir > tbody > tr:not(:first-child)').remove();
			var response = null;
			$.ajax({
		    	url: urlToContact + "/api/reservations-date?dateResa=" + selectedDate,
		    	headers: {
		    		'Authorization': 'Bearer ' + tokenUser,
		    	},
		    	method: 'GET',
		    	success: function(response){
	    			sendResultsResasByDate(response, selectedDate);
		    	}
		    });
		};
		
		
		
		
		
		//function to get all reservations by date and process data
		function sendResultsResasByDate(responseAjax, selectedDate){
			
			let resasTotalMidi = 0;
			let resasTotalSoir = 0;
			let resasTablesMidi = 0;
			let resasTablesSoir = 0;
			let resaCreneau1Midi = 0;
			let resaCreneau2Midi = 0;
			let resaCreneau3Midi = 0;
			let resaCreneau1Soir = 0;
			let resaCreneau2Soir = 0;
			let resaCreneau3Soir = 0;
			let tablesMidi = [];
			let tablesSoir = [];
			$('.fermeture-accueil').remove();
			
			
			
			// get date and display it
			afficherDateSurPage(selectedDate, "#dateResaConsultation");
			
			
			if(responseAjax == null || responseAjax == undefined){
				resasTotalMidi = 0;
				resasTotalSoir = 0;
				
			}else{
	
				responseAjax.forEach(el => {
					var notesMidi = document.getElementById('notes-midi');
					var notesSoir = document.getElementById('notes-soir');
					
					if(el.serviceResa == "midi"){
						tablesMidi.push(el.nbCouvertsResa);
						
						resasTotalMidi += el.nbCouvertsResa;
						resasTablesMidi++;
						
						if(el.creneauResa.idCreneau == creneaux.midi1.idCreneau){
							resaCreneau1Midi += el.nbCouvertsResa;
						}
						if(creneaux.midi2 != null && el.creneauResa.idCreneau == creneaux.midi2.idCreneau){
							resaCreneau2Midi += el.nbCouvertsResa;
						}
						if(creneaux.midi3 != null && el.creneauResa.idCreneau == creneaux.midi3.idCreneau){
							resaCreneau3Midi += el.nbCouvertsResa;
						}
						if(el.notesResaImportant == true){
							let row = notesMidi.insertRow(-1);
							 let cell1 = row.insertCell(0);
							 let cell2 = row.insertCell(1);
							 cell1.innerText = el.nomResa;
							 cell2.innerText = el.notesResa;
						}
					}else if(el.serviceResa == "soir"){
						tablesSoir.push(el.nbCouvertsResa);
						
						resasTotalSoir += el.nbCouvertsResa;
						resasTablesSoir++;
						
						if(el.creneauResa.idCreneau == creneaux.soir1.idCreneau){
							resaCreneau1Soir += el.nbCouvertsResa;
						}
						if(creneaux.soir2 != null && el.creneauResa.idCreneau == creneaux.soir2.idCreneau){
							resaCreneau2Soir += el.nbCouvertsResa;
						}
						if(creneaux.soir3 != null && el.creneauResa.idCreneau == creneaux.soir3.idCreneau){
							resaCreneau3Soir += el.nbCouvertsResa;
						}
						
						if(el.notesResaImportant == true){
							let row = notesSoir.insertRow(-1);
							 let cell1 = row.insertCell(0);
							 let cell2 = row.insertCell(1);
							 cell1.innerText = el.nomResa;
							 cell2.innerText = el.notesResa;
						}
					}
					
					
				});
				
			}
			
			//fill tables and booking details in table
			var tablesMidiTri = {};
			var tablesSoirTri = {};
			tablesMidi.map( function (a) { if (a in tablesMidiTri) tablesMidiTri[a] ++; else tablesMidiTri[a] = 1; } );
			tablesSoir.map( function (a) { if (a in tablesSoirTri) tablesSoirTri[a] ++; else tablesSoirTri[a] = 1; } );
			$.each(tablesMidiTri, function(index, value){
				var parafBlocMidi = document.createElement("P");
				parafBlocMidi.innerText = value + " X " + index + " couvert(s)";
				$("#tablesResasMidi").append(parafBlocMidi);
			});
			
			$.each(tablesSoirTri, function(index, value){
				var parafBlocSoir = document.createElement("P");
				parafBlocSoir.innerText = value + " X " + index + " couvert(s)";
				$("#tablesResasSoir").append(parafBlocSoir);
			});
			
			console.log(creneaux.midi1);
			console.log(creneaux.soir1);
			//fill all the fields
			
			//reset all fields
			$("#hCreneau1Midi, #hCreneau2Midi, #hCreneau3Midi, #hCreneau1Soir, #hCreneau2Soir, #hCreneau3Soir").text("");
			$("#resaCreneau1Midi, #resaCreneau2Midi, #resaCreneau3Midi, #resaCreneau1Soir, #resaCreneau2Soir, #resaCreneau3Soir").text("");
			$(".hidden-creneau-midi1, .hidden-creneau-midi2, .hidden-creneau-midi3, .hidden-creneau-soir1, .hidden-creneau-soir2, .hidden-creneau-soir3").hide();
			$("#couvertsJourMidi").text(resasTotalMidi);
			$("#couvertsJourSoir").text(resasTotalSoir);
			$("#resasTablesMidi").text(resasTablesMidi);
			$("#resasTablesSoir").text(resasTablesSoir);
			if(creneaux.midi1 != undefined){
				$("#hCreneau1Midi").text(creneaux.midi1.creneauDebut.substring(0,5) + " - " + creneaux.midi1.creneauFin.substring(0,5));
				$("#resaCreneau1Midi").text(resaCreneau1Midi);
				if(creneaux.midi2 != null){
					$(".hidden-creneau-midi2").show();
					$("#hCreneau2Midi").text(creneaux.midi2.creneauDebut.substring(0,5) + " - " + creneaux.midi2.creneauFin.substring(0,5));
					$("#resaCreneau2Midi").text(resaCreneau2Midi);
				}else{
					$(".hidden-creneau-midi2").hide();
				}
				if(creneaux.midi3 != null){
					$(".hidden-creneau-midi3").show();
					$("#hCreneau3Midi").text(creneaux.midi3.creneauDebut.substring(0,5) + " - " + creneaux.midi3.creneauFin.substring(0,5));
					$("#resaCreneau3Midi").text(resaCreneau3Midi);
				}else{
					$(".hidden-creneau-midi3").hide();
				}
			}else{
				$('#container-midi').append("<span class='fermeture-accueil m20'>(FERME)</span>");
			}
			
			if(creneaux.soir1 != undefined){
				$("#resaCreneau1Soir").text(resaCreneau1Soir);
				$("#hCreneau1Soir").text(creneaux.soir1.creneauDebut.substring(0,5) + " - " + creneaux.soir1.creneauFin.substring(0,5));
				if(creneaux.soir2 != null){
					$(".hidden-creneau-soir2").show();
					$("#hCreneau2Soir").text(creneaux.soir2.creneauDebut.substring(0,5) + " - " + creneaux.soir2.creneauFin.substring(0,5));
					$("#resaCreneau2Soir").text(resaCreneau2Soir);
				}else{
					$(".hidden-creneau-soir2").hide();
				}
				if(creneaux.soir3 != null){
					$(".hidden-creneau-soir3").show();
					$("#hCreneau3Soir").text(creneaux.soir3.creneauDebut.substring(0,5) + " - " + creneaux.soir3.creneauFin.substring(0,5));
					$("#resaCreneau3Soir").text(resaCreneau3Soir);
				}else{
					$(".hidden-creneau-soir3").hide();
				}
			}else{
				$('#container-soir').append("<span class='fermeture-accueil m20'>(FERME)</span>");
			}
			
		};
	
	}
	
	if(pathname == CONTEXT_PATH+"/reservation"){
		let joursOuverts = [];
		getJoursOuverture();
		
		//function to check general opening days
		function getJoursOuverture(){
			let userToken = $("#tokenJwt").val();

			 getJoursOuverts=$.ajax({
	            url: urlToContact + "/api/parametres-jours-ouverts",
	            headers: {
			    		'Authorization': 'Bearer ' + userToken,
	            },
	            data: {
	                method: 'GET', 
	                dataType: 'json', 
	            },
	            success: function( result ) {
	                
	            }
	        });
			 
			 getJoursOuverts.done(function(result){
				 joursOuverts = result;
				 getJoursOuvertureParticuliers();
			 });
		}
		
		
		//function to check exceptionnal opening days
		function getJoursOuvertureParticuliers(){
			let userToken = $("#tokenJwt").val();

			 getJoursOuvertsParticulier=$.ajax({
	            url: urlToContact + "/api/all-parametres-particuliers",
	            headers: {
			    		'Authorization': 'Bearer ' + userToken,
	            },
	            data: {
	                method: 'GET', 
	                dataType: 'json', 
	            },
	            success: function( result ) {
	                
	            }
	        });
			 
			 getJoursOuvertsParticulier.done(function(result){
				 let today = Date.now();
				 let listOfDays = [];
				 if(result != undefined){
					 result.forEach(el => {
						 let dateToCompare = Date.parse(el.idParamP);
						 if(dateToCompare >= today && (el.ouvertMidiP || el.ouvertSoirP)){
							 listOfDays.push(el.idParamP);
						 }
						 
					 });
				 }
				 console.log(joursOuverts);
				 $( "#datepicker" ).datepicker({
				    	dateFormat: "yy-mm-dd",
				    	numberOfMonths: 2,
				    	minDate:0,
				    	changeMonth: true,
				  		changeYear: true,
				  		beforeShowDay: function(date){
				  			let day = date.getDay();
				  			let string = jQuery.datepicker.formatDate('yy-mm-dd', date);
				  			if(joursOuverts[day] == false && listOfDays.indexOf(string) == -1){
				  				return [false]
				  			}
				  			return [true]
				  	    }
				    });
				 
				 	//on reload page if form has errors, get date used in the datepicker to put it back
					if($(".message-erreur").length > 0 || $("#idResa").val() > 0){
						var dateToKeep = $("#dateAReserver").attr("value");
						$( "#datepicker" ).datepicker( "setDate", dateToKeep );
					}else{
						//get actual date on load
						var currentDate = $( "#datepicker" ).val();
						$("#dateAReserver").attr("value", currentDate);
					}
			 });
		}
		
		
		
		
		//change date in form by selected date in date picker
		$("#datepicker").on("change",function(){
       	 	$("#creneauResaId")
       	 		.empty()
       	 		.append('<option value="0" selected="selected"> --Sélectionnez--</option>');;
       	 	
	        var selected = $(this).val();
	        getCreneauxByDateAjax(selected);
	        $("#dateAReserver").attr("value", selected);
	    });

		//function to get créneaux by date and availability ajax
		function getCreneauxByDateAjax(date){
			let userToken = $("#tokenJwt").val();
			//fill dropdown crénaux with available ones
			 xhr=$.ajax({
	            url: urlToContact + "/api/creneaux-date?date=" + date,
	            headers: {
			    		'Authorization': 'Bearer ' + userToken,
	            },
	            data: {
	                method: 'GET', 
	                dataType: 'json', 
	            },
	            success: function( result ) {
	                
	            }
	        });

	        //dropdown créneaux maker
	        xhr.done(function(result){
	       	 let creneaux = document.getElementById('creneauResaId');
	       	 if(result != undefined){
		       	 result.forEach(element => {
		       		 console.log(element);
		       		 let tag = document.createElement("option");
		       		 tag.setAttribute("value", element.idCreneau);
		       		 tag.textContent = element.creneauDebut + ' - ' + element.creneauFin;
		       		 creneaux.append(tag);
		       	 }); 
	       	 }
	        });
		}
                        
		//reset option 0 select creneau
		$("#creneauResaId > option:first-child").attr("value", "");
		$("#nbCouvertsResa").attr("value", "");
		
		//check form fields client side
		///////////////////////////////////////
		$("#validFormResa").click(function(){
			if(!verifForm()){
				return false;
			}else{
				return true;
			}
		});
		///////////////////////////////////////
		
		
		// function to check form client side
		function verifForm(){
            var formulaire = document.getElementsByClassName('formResaItem');

            for(i = 0; i < formulaire.length; i++){
                testBloc(formulaire[i]);                  
            }
            if(document.querySelectorAll('div.erreur-champ').length > 0){
                document.querySelectorAll('.erreur-champ input, .erreur-champ select')[0].focus();

                return false;
            }else{
                return true;
            }

        }
		
		//function to check all form fields
		function testBloc(blocTested) {
			 if(blocTested.getAttribute('type') === 'text' || blocTested.getAttribute('type') === 'number')
			 {
                 if(blocTested.value.trim() === ""){
                     testBlocFalse(blocTested);
                 }else{
                     testBlocTrue(blocTested);
             }
                     
             }else if(blocTested.tagName === 'SELECT')
             {
                 if(blocTested.value == 0){
                     testBlocFalse(blocTested);
                 }else{
                     testBlocTrue(blocTested);
                 }
             }
		}
		
		//function to notify fail form submission
		function testBlocFalse(bloc){               
            bloc.parentNode.classList.add('erreur-champ');
        }

        function testBlocTrue(bloc){
            bloc.parentNode.classList.remove('erreur-champ');
        }
	}
	
	if(pathname == CONTEXT_PATH+"/reservations-consulter"){
		$( "#consulterdatepicker" ).datepicker({
	    	dateFormat: "yy-mm-dd",
	    	numberOfMonths: 2,
	    	changeMonth: true,
	  		changeYear: true,
	    });
		var dateSelect;
		// get date and date on load page
		var currentDate = $( "#consulterdatepicker" ).val();
		ajaxGetAllResas(currentDate);
		afficherDateSurPage(currentDate, "#dateAffichageReservations");
		dateSelect = currentDate;
		
		// get date and datas clicking on date in datepicker
		$("#consulterdatepicker").on("change",function(){
			$("#tbody-reservations-soir > tr").remove();
        	$("#tbody-reservations-midi > tr").remove();
	        var selected = $(this).val();
	        
	        if(selected != dateSelect){
		        ajaxGetAllResas(selected);
		        afficherDateSurPage(selected, "#dateAffichageReservations");
		        dateSelect = selected;
	        }else{
	        	ajaxGetAllResas(selected);
	        }
	    });
		
		
		
		//ajax request to get all reservations by date 
		function ajaxGetAllResas(dateSelected){
			
			xhr=$.ajax({
	            url: urlToContact + "/api/reservations-date?dateResa="+dateSelected,
	            headers: {
			    		'Authorization': 'Bearer ' + tokenUser,
	            },
	            data: {
	                method: 'GET', 
	                dataType: 'json', 
	            },
	            success: function( result ) {
	            	
	            }
	        });
			
			//creneaux dropdown maker
	         xhr.done(function(result){
	        	 if(result == undefined){
	        		 $('#aucuneReservation')
	        		 	.text("Aucune réservation pour cette date")
	        		 	.addClass("alert alert-warning");
	        		 
	        	 }else{
	        		 $('#aucuneReservation')
	        		 .text("")
	        		 .removeClass("alert alert-warning");
	        		 var tablesMidi = document.getElementById('tbody-reservations-midi');
	        		 var tablesSoir = document.getElementById('tbody-reservations-soir');
	        		 
	        		 //sorting reservations by date
	        		 result.sort(function(r1, r2){
	        			 return (r1.creneauResa.creneauDebut).localeCompare(r2.creneauResa.creneauDebut);
	        		 });
	        		 
	        		 // variable to change creneaux in dropdown in reservation form 
	        		 let actualCreneau = "0";
	        		 let changeCreneau = false;
	        		 result.forEach(element => {
	        			 if(element.creneauResa.creneauDebut > actualCreneau){
	        				 if(actualCreneau > 0){
	        					 changeCreneau = true;
	        				 }
	        				 actualCreneau = element.creneauResa.creneauDebut;
	        			 }
	        			 if(element.serviceResa == "midi"){
	        				 alimReservations(tablesMidi, element);
	        			 }else if(element.serviceResa == "soir"){
	        				 alimReservations(tablesSoir, element);
	        			 }

	            	 });
	        	 }
	        	 
	        	 // call to delete confirmation modal 
	        	 $(document).on('click', '.del-reservation', function(e){
	        		e.preventDefault();
	        		var elementASupprimer = $(this).parents('tr');
	     			var idResaASupprimer = elementASupprimer.data('id-resa');
		        	 
		            	             
		           //function to deal with delete
			         $("#dialog-confirm").dialog({
			             autoOpen: false,
			             modal: true,
			             buttons: {
			                 "SUPPRIMER": function() {
			                	 var url = context + "/reservation-suppression";
			                	 var form = $('<form action="' + url + '" method="post">' +
			                			  '<input type="number" name="idsuppr" value="' + idResaASupprimer + '" />' +
			                	  '</form>');
			                	 $('body').append(form);
			                	 form.submit();
			                 },
			                 "Annuler": function() {
			                   $( this ).dialog( "close" );
			                 }
			               }
			           });
			         
			         $("#dialog-confirm").dialog("open");
			         
		         });
	        	 
	        	 $(document).on('click', ".update-reservation", function(){
	        		 
		 			var formResa = "";
		 			var elementAEnvoyer = $(this).parents('tr');
		 			var idResa = elementAEnvoyer.data('id-resa');
		 			window.location = context + "/reservation?idResa=" + idResa;
		 		});
	        	 
	         });
	         
	         
	         
	         
	        
	         
	         
	         
		}

		
	}
	
	if(pathname == CONTEXT_PATH+"/set-params"){
		let formParams = document.querySelector("#parametresDto");
		formParams.addEventListener("submit", checkForm);
		
		function checkForm(e){
			let params = this.querySelectorAll('.params-item');
			params.forEach((x) => {
				let selects = x.querySelectorAll('select');
				console.log(selects);
				if((selects[2].value != "" && selects[3].value == "") || (selects[2].value == "" && selects[3].value != "")){
					selects[2].setAttribute('required', 'required');
					selects[2].classList.add("erreurParam");
					selects[3].setAttribute('required', 'required');
					selects[3].classList.add("erreurParam");
					e.preventDefault();	
				}
				if((selects[4].value != "" && selects[5].value == "") || (selects[4].value == "" && selects[5].value != "")){
					selects[4].setAttribute('required', 'required');
					selects[4].classList.add("erreurParam");
					selects[5].setAttribute('required', 'required');
					selects[5].classList.add("erreurParam");
					e.preventDefault();	
				}
				selects.forEach((y) => {
					console.log(y.value);
				});
			});
			
		}
	}
	
	if(pathname == CONTEXT_PATH+"/parametres-particuliers"){
		var validationPpFormulaire = $("#isFormValid").val();
		var isMultiForm = $("#isMulti").val();
		
		var tabDatesMulti = "";
		var tabDatesPlage = "";
		$("#tabDates").val() == "";
		
		let tabDatesParametrees = [];
		


		
		
		
		
		//ajax to get all parametres particuliers and display dates already set in datepicker
		ajaxGetAllParametresParticuliers();
		
		//ajax request to get all dates which have parametres particuliers set and then display datepickers with function to displays parameters on already sets dates
		function ajaxGetAllParametresParticuliers(){
			
			xhr=$.ajax({
	            url: urlToContact + "/api/all-parametres-particuliers",
	            headers: {
			    		'Authorization': 'Bearer ' + tokenUser,
	            },
	            data: {
	                method: 'GET', 
	                dataType: 'json', 
	            },
	            success: function( result ) {
	            	
	            }
	        });
			
			xhr.done(function(result){
				if(result != undefined){
					result.forEach(element => {
						let d = new Date(element.idParamP);
						d.setHours(0,0,0,0);
						tabDatesParametrees.push(d.getTime());
	            	});
				}
				
				$( "#datepicker-pp" ).multiDatesPicker({
			    	dateFormat: "yy-mm-dd",
			    	numberOfMonths: 2,
			    	changeMonth: true,
			  		changeYear: true,
			  		beforeShowDay: function(date){
			  			let dateToFind = date.getTime();
			  			if($.inArray(dateToFind, tabDatesParametrees) > -1){
			  				return [true, 'red-highlight', ''];
			  			}else{
				  			return [true, '', ''];
			  			}
			  			
			  	    },
			  	    onSelect: function(date, el){
			  	    	
			  	    	var day  = el.selectedDay,
	                    mon  = el.selectedMonth,
	                    year = el.selectedYear;

		                var el = $(el.dpDiv).find('[data-year="'+year+'"][data-month="'+mon+'"]').filter(function() {
		                    return $(this).find('a').text().trim() == day;
		                });
		                
		                if ( el.hasClass('red-highlight') && !$(this).hasClass("datepick-selected")){
		                	displayDateParameters(date);
		                } else {
		                	$("#infosParametres").hide();
		                }
			  	    }
			    });
			});
		}
		
		//on click on a date which already has parametres particuliers, check for them to display
		function displayDateParameters(date){
			
			console.log("function display : " + date);
			checkParametresParticulier(date);
			$("#infosParametres").show();
		}

		
		function checkParametresParticulier(date){
			 getParametreType=$.ajax({
				 url: urlToContact + "/api/parametres-particuliers?dateParamPDate1=" + date,
				 headers: {
					 'Authorization': 'Bearer ' + tokenUser,
				 },
				 data: {
	                 method: 'GET',
	                 dataType: 'json', 
	             },
	             success: function( result ) {
	                	getSingleParametreParticulier(result);
	             }
			 });
		 }
		
		//on click on a date wich has parametres particuliers -> displays params
		function getSingleParametreParticulier(result){
			if(result != undefined){
				let parametresARemplir = document.getElementById('parametresParticulier');
				$("#parametresParticulier > tbody > tr:not(:first-child)").remove();
				alimParametresParticulier(parametresARemplir, result[0]);
			}
		}
		
		
		//form submit -> get date to feed input field
		$("#form-pp").submit(function(e){

			$("#isMulti").val(true);
			
			let multiDates = $('#datepicker-pp').multiDatesPicker('getDates');
			console.log(multiDates);
			
			if(multiDates.length > 0){
				$("#tabDates").val(multiDates);
			}else{
				$("#dates-errors").show();
				e.preventDefault();
			}
			
		});
		

		
//		var tokenUser = "${userToken}";
	}
	
	if(pathname == CONTEXT_PATH+"/rapports"){
		$('#soloDatePicker').datepick($.extend({ 
		    showTrigger: '#calImg', 
		    altField: '#l10nAlternate', altFormat: 'yyyy-mm-dd',
		    rangeSelect: false,
		    defaultDate: 0,
		    dateFormat: 'yyyy-mm-dd'}));
		
		$('#rangeDatePicker').datepick($.extend({ 
		    showTrigger: '#calImg', 
		    altField: '#l10nAlternate', altFormat: 'yyyy-mm-dd',
		    rangeSelect: true, monthsToShow: [1, 2],
		    dateFormat: 'yyyy-mm-dd'}, 
		    $.datepick.regionalOptions['fr']));
		
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();

		today = yyyy + "-" + mm + "-" + dd;
		$('#dateSoloRapport').val(today);
		$('#dateDebutRapport').val(today);
		$('#dateFinRapport').val(today);
		
		$('#soloDatePicker').on('click', function(){
			let dateSelected = $(this).datepick('getDate')[0];
			$('#dateSoloRapport').val(dateSelected.yyyymmdd());
		});
		
		$('#rangeDatePicker').on('click', function(){
			let dateDebut = $(this).datepick('getDate')[0];
			let dateFin = $(this).datepick('getDate')[1]
			$('#dateDebutRapport').val(dateDebut.yyyymmdd());
			$('#dateFinRapport').val(dateFin.yyyymmdd());
		});
	}
	
	
	/////////////shared general functions/////////////////////
	
	//function to display correctly the date
	function afficherDateSurPage(dateAAfficher, idBlocAffichage){
		let dateToDisplay = new Date(dateAAfficher);
		const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
		$(idBlocAffichage).text(dateToDisplay.toLocaleDateString('fr-FR', options));
	}
	
	function alimReservations(destination, element){
		
		//creation rows for table
		 var row = destination.insertRow(-1);
		 var cell1 = row.insertCell(0);
		 var cell2 = row.insertCell(1);
		 var cell3 = row.insertCell(2);
		 var cell4 = row.insertCell(3);
		 var cell5 = row.insertCell(4);
		 var cell6 = row.insertCell(5);
		 var cell8 = row.insertCell(6);
		 
		 //fill cells
		 row.dataset.idResa = element.idResa;
		 cell1.innerText = element.creneauResa.creneauDebut + ' - ' + element.creneauResa.creneauFin;
		 cell1.dataset.idCreneau = element.creneauResa.idCreneau;
		 cell2.innerText = element.nomResa;
		 cell3.innerText = element.telephoneResa;
		 cell4.innerText = element.mailResa;
		 cell5.innerText = element.nbCouvertsResa;
		 cell5.classList.add("text-center");
		 cell6.innerText = element.notesResa;

		 //add/remove class by boolean
		 if(element.habitue){
			 cell2.classList.add("est-habitue");
		 }else{
			 cell2.classList.remove("est-habitue");
		 }
		 if(element.notesResaImportant) {
			 cell6.classList.add("est-important");
		 }else{
			 cell6.classList.remove("est-important");
		 }
		
		 //buttons creation
		 if(Date.parse(element.dateResa) >= dateToday.getTime()){
			 var buttonUpdate = document.createElement("BUTTON");
			 buttonUpdate.innerHTML = "<img src='/resources/img/crayon.svg'>";
			 buttonUpdate.classList.add("alert", "update-reservation");
			 
			 var buttonDelete = document.createElement("BUTTON");
			 buttonDelete.innerHTML = "<img src='/resources/img/nok.svg'>";
			 buttonDelete.classList.add("alert", "del-reservation");
			 
			 cell8.append(buttonUpdate);
			 cell8.append(buttonDelete);
		 
		 }else{
			 cell8.innerText = "Date passée";
		 }
	}
	
	//function to fill parametres particuliers with data from selected day
	function alimParametresParticulier(destination, element){
		var row = destination.insertRow(-1);
		 var cell1 = row.insertCell(0);
		 var cell2 = row.insertCell(1);
		 var cell3 = row.insertCell(2);
		 var cell4 = row.insertCell(3);
		 var cell5 = row.insertCell(4);
		 var cell6 = row.insertCell(5);
		 var cell7 = row.insertCell(6);
		 var cell8 = row.insertCell(7);
		 var cell9 = row.insertCell(8);
		 var ouvertMidi = document.createElement('input');
		 	ouvertMidi.type = 'checkbox';
		 	ouvertMidi.class = "pp-isOuvertMidi";
		 	ouvertMidi.disabled = true;
	    var ouvertSoir = document.createElement('input');
		    ouvertSoir.type = 'checkbox';
		    ouvertSoir.class = "pp-isOUvertSoir";
		    ouvertSoir.disabled = true;
		    
		    cell1.innerText = element.idParamP.substring(8) + "-" + element.idParamP.substring(5,7) + "-" + element.idParamP.substring(0,4);
		    cell2.appendChild(ouvertMidi);
		    if(element.ouvertMidiP){
		    	ouvertMidi.checked = true;
		    }else{
		    	ouvertMidi.checked = false;
		    }
		    cell3.appendChild(ouvertSoir);
		    if(element.ouvertSoirP){
		    	ouvertSoir.checked = true;
		    }else{
		    	ouvertSoir.checked = false;
		    }
		    cell4.innerText = (element.ouvertMidiP  ? element.creneau1Midi.creneauDebut + " - " + element.creneau1Midi.creneauDebut : "N/A");
		    cell5.innerText = (element.creneau2Midi != null ? element.creneau2Midi.creneauDebut + " - " + element.creneau2Midi.creneauDebut : "N/A");
		    cell6.innerText = (element.creneau3Midi != null ? element.creneau3Midi.creneauDebut + " - " + element.creneau3Midi.creneauDebut : "N/A");
		    cell7.innerText = (element.ouvertSoirP ? element.creneau1Soir.creneauDebut + " - " + element.creneau1Soir.creneauDebut : "N/A");
		    cell8.innerText = (element.creneau2Soir != null ? element.creneau2Soir.creneauDebut + " - " + element.creneau2Soir.creneauDebut : "N/A");
		    cell9.innerText = (element.creneau2Soir != null ? element.creneau3Soir.creneauDebut + " - " + element.creneau3Soir.creneauDebut : "N/A");
	}
	
	//function to get parameter in URL
	function getUrlParameter(sParam) {
	    var sPageURL = window.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }
	};
	
	//function to format dates to set picker if page reloaded after failed form submit on parametres particuliers
	function setPickersOnErrorForm(getDates){
		var getDatesOk = getDates.replace(/-/g, '/');
		var getDatesString = getDatesOk.toString().split(',');
		var datesToSet = [];
		getDatesString.forEach(element => datesToSet.push(element.split("/").reverse().join("/")));
		
		return datesToSet;
	}
	
	//function to search dates in dates array
	function isInArray(array, value) {
		  return !!array.find(item => {return item.getTime() == value.getTime()});
	}
	
	//function to transform date to string to pass it to the API
	Date.prototype.yyyymmdd = function() {
		  var mm = this.getMonth() + 1; // getMonth() is zero-based
		  var dd = this.getDate();

		  return [this.getFullYear() + "-",
		          (mm>9 ? '' : '0') + mm + "-",
		          (dd>9 ? '' : '0') + dd
		         ].join('');
		};
	
});