<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<script src="<c:url value='/resources/js/jquery-3.5.js' />"></script>
	<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
	<script src="<c:url value='/resources/js/jqueryui.js' />" ></script>
	<script src="<c:url value='/resources/js/jquery-ui-fr.js' />" ></script>
	<script>
		var tokenUser = "${userToken}";
	</script>
   		<!-- <script src="${contextPath}/resources/js/jquery-ui.multidatespicker.js" ></script> -->
   		<script type="text/javascript" src="<c:url value='/resources/js/jquery.plugin.js' />"></script> 
		<script type="text/javascript" src="<c:url value='/resources/js/jquery.datepick.js' />"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/jquery.datepick-fr.js' />"></script>
		<script type="text/javascript" src="<c:url value='/resources/js/jquery-ui.multidatespicker.js' />"></script>
	<input type=hidden value="${userToken}" id="tokenJwt"/>
	<script src="<c:url value='/resources/js/custom.js' />" ></script>
	<script>var ctx = "${pageContext.request.contextPath}"</script>
</body>
</html>