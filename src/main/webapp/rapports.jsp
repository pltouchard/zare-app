<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<jsp:include  page="/header.jsp"></jsp:include>

	<div id="form-generation-rapports-jour" class="container">
	
		<h2>Rapport quotidien des réservations</h2>
		<p classe=>Merci de choisir un jour pour générer un rapport sur les réservations</p>
		<form class="flex-col-center" action="<c:url value='/generation-rapport-json-jour.xlsx' />" method="GET">
			<div id="date-rapport">
				<div id="soloDatePicker"></div>
				<input id="dateSoloRapport" type="hidden" name="dateResa" value="" />
			</div>
		  <input class="ml40"  type="submit" value="Générer">
		</form>	
	</div>
	
	<div id="form-generation-rapports-plage" class="container">
	
		<h2>Rapport d'activité</h2>
		<p>Merci de choisir une date ou plage de date pour générer un rapport sur l'activité</p>
		<form class="flex-col-center" action="<c:url value='/generation-rapport-json-activite.xlsx' />" method="GET">
			<div id="range-rapport">
				<div id="rangeDatePicker"></div>
				<input id="dateDebutRapport" type="hidden" name="dateDebut" value="" />
				<input id="dateFinRapport" type="hidden" name="dateFin" value="" />
			</div>
		  <input class="ml40"  type="submit" value="Générer">
		</form>	
	</div>

<jsp:include  page="/footer.jsp"></jsp:include>