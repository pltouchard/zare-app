package com.projetZare.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projetZare.auth.model.ApplicationInfos;

public interface ApplicationInfosRepository extends JpaRepository<ApplicationInfos, Integer> {

}
