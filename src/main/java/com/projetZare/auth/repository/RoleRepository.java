package com.projetZare.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projetZare.auth.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
