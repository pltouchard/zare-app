package com.projetZare.auth.exceptions;

public class ApiRequestErrorException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ApiRequestErrorException(String errorMessage) {
		super(errorMessage);
	}
}
