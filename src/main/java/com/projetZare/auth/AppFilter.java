package com.projetZare.auth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.projetZare.auth.model.Parametres;
import com.projetZare.auth.service.impl.ParametresServiceImpl;
import com.projetZare.auth.service.impl.SecurityServiceImpl;

@Component
public class AppFilter implements Filter {
	
	@Autowired
	ParametresServiceImpl parametresService;
	
	@Autowired
	SecurityServiceImpl securityServiceImpl;
	
   @Override
   public void destroy() {}

   @Override
   public void doFilter
      (ServletRequest request, ServletResponse response, FilterChain filterchain) 
      throws IOException, ServletException {
	   ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    HttpSession httpSession = attr.getRequest().getSession(true); 
	   HttpServletRequest httpRequest = (HttpServletRequest) request;
       String requestURI = httpRequest.getRequestURI();
       Object token = null;
		 try {
			 if(token == null) {
				 String getToken = securityServiceImpl.call_me();
				 httpSession.setAttribute("jwtToken", getToken);
			 }
			 
			 		
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	//((HttpServletResponse)response).sendRedirect(httpRequest.getContextPath() + "/login?deconnection"); 
	    }
       Parametres isParamsSet = parametresService.getParametres(httpSession);
       
	   if(isParamsSet == null && !requestURI.endsWith("/login")  && !requestURI.endsWith("/set-params") && !requestURI.contains("resources")) {
		  
		   ((HttpServletResponse)response).sendRedirect(httpRequest.getContextPath() + "/set-params"); 
	   }else{  
    	   filterchain.doFilter(request,response);  
       }  
   }

   @Override
   public void init(FilterConfig filterconfig) throws ServletException {}
}
