package com.projetZare.auth.service;

import com.projetZare.auth.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
