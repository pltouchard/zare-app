package com.projetZare.auth.service;

import java.net.http.HttpResponse;
import java.text.ParseException;

import javax.servlet.http.HttpSession;

import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.ParametreParticulierDto;

public interface ParametreParticulierService {
	public HttpResponse<String> saveParametresPaticuliers(ParametreParticulierDto parametreParticulierDto, HttpSession httpSession) throws ParseException;
	
	public Creneau getCreneauByValues(HttpSession httpSession, String time1, String time2) throws ParseException;
}
