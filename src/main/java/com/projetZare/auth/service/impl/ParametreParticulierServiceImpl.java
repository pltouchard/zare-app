package com.projetZare.auth.service.impl;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projetZare.auth.exceptions.ApiRequestErrorException;
import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.ParametreParticulierDto;
import com.projetZare.auth.model.ParametresParticuliers;
import com.projetZare.auth.model.ParametresParticuliersDto;
import com.projetZare.auth.service.ParametreParticulierService;
import com.projetZare.auth.service.api.ApiCallsService;
import com.projetZare.auth.service.api.ApiCallsServiceParametresParticuliers;

@Service
public class ParametreParticulierServiceImpl implements ParametreParticulierService {
	
	@Autowired
	ApiCallsService apiCallsService;
	
	@Autowired
	ApiCallsServiceParametresParticuliers apiCallsServiceParametresParticuliers;

	@Override
	public HttpResponse<String> saveParametresPaticuliers(ParametreParticulierDto parametreParticulierDto,
			HttpSession httpSession) throws ParseException {
		
		ArrayList<String> datesPp = new ArrayList<String>(Arrays.asList(parametreParticulierDto.getDates().split(",")));
		ParametresParticuliersDto parametresParticuliersDto = new ParametresParticuliersDto();
		ParametresParticuliers parametresParticuliers = new ParametresParticuliers();
		
		//add parameters ok
		parametresParticuliersDto.setDates(datesPp);
		parametresParticuliers.setOuvertMidiP(parametreParticulierDto.getOuvertMidiP());
		parametresParticuliers.setOuvertSoirP(parametreParticulierDto.getOuvertSoirP());
		
		
		//check Creneaux -> if exists, retrieve it / if not, save it !
		if(parametreParticulierDto.getOuvertMidiP() == true) {
			
			parametresParticuliers.setCreneau1Midi(getCreneauByValues(httpSession,parametreParticulierDto.getCreneau1MidiDebut(), parametreParticulierDto.getCreneau1MidiFin()));
			parametresParticuliers.setCreneau1MidiNbResas(parametreParticulierDto.getCreneau1MidiNbResas());
			
		}else {
			//fake values to bypass the obligation to have at least one creneau for noon and the evening set for parametres generaux
			String fakeDebutMidi = "00:01:00";
			String fakeFinMidi = "00:02:00";
			parametresParticuliers.setCreneau1Midi(getCreneauByValues(httpSession, fakeDebutMidi, fakeFinMidi));
		}
		
		if(parametreParticulierDto.getOuvertSoirP() == true) {
			parametresParticuliers.setCreneau1Soir(getCreneauByValues(httpSession,parametreParticulierDto.getCreneau1SoirDebut(), parametreParticulierDto.getCreneau1SoirFin()));
			parametresParticuliers.setCreneau1SoirNbResas(parametreParticulierDto.getCreneau1SoirNbResas());
		}else {
			//fake values to bypass the obligation to have at least one creneau for noon and the evening set for parametres generaux
			String fakeDebutSoir = "00:03:00";
			String fakeFinSoir = "00:04:00";
			parametresParticuliers.setCreneau1Soir(getCreneauByValues(httpSession, fakeDebutSoir, fakeFinSoir));
		}
		
		if(parametreParticulierDto.getCreneau2MidiDebut() != "" && parametreParticulierDto.getCreneau2MidiFin() != "") {
			parametresParticuliers.setCreneau2Midi(getCreneauByValues(httpSession,parametreParticulierDto.getCreneau2MidiDebut(), parametreParticulierDto.getCreneau2MidiFin()));
			parametresParticuliers.setCreneau2MidiNbResas(parametreParticulierDto.getCreneau2MidiNbResas());
		}else {
			parametresParticuliers.setCreneau2Midi(null);
		}
		
		if(parametreParticulierDto.getCreneau3MidiDebut() != "" && parametreParticulierDto.getCreneau3MidiFin() != "") {
			parametresParticuliers.setCreneau3Midi(getCreneauByValues(httpSession,parametreParticulierDto.getCreneau3MidiDebut(), parametreParticulierDto.getCreneau3MidiFin()));
			parametresParticuliers.setCreneau3MidiNbResas(parametreParticulierDto.getCreneau3MidiNbResas());
		}else {
			parametresParticuliers.setCreneau3Midi(null);
		}
		
		if(parametreParticulierDto.getCreneau2SoirDebut() != "" && parametreParticulierDto.getCreneau2SoirFin() != "") {
			parametresParticuliers.setCreneau2Soir(getCreneauByValues(httpSession,parametreParticulierDto.getCreneau2SoirDebut(), parametreParticulierDto.getCreneau2SoirFin()));
			parametresParticuliers.setCreneau2SoirNbResas(parametreParticulierDto.getCreneau2SoirNbResas());
		}else {
			parametresParticuliers.setCreneau2Soir(null);
		}
		
		if(parametreParticulierDto.getCreneau3SoirDebut() != "" && parametreParticulierDto.getCreneau3SoirFin() != "") {
			parametresParticuliers.setCreneau3Soir(getCreneauByValues(httpSession,parametreParticulierDto.getCreneau3SoirDebut(), parametreParticulierDto.getCreneau3SoirFin()));
			parametresParticuliers.setCreneau3SoirNbResas(parametreParticulierDto.getCreneau3SoirNbResas());
		}else {
			parametresParticuliers.setCreneau3Soir(null);
		}
		
		parametresParticuliersDto.setParametreParticulier(parametresParticuliers);
				
		if(parametreParticulierDto.isMulti()) {
			try {
				return apiCallsServiceParametresParticuliers.saveParametresParticuliersMulti(httpSession, parametresParticuliersDto);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			try {
				return apiCallsServiceParametresParticuliers.saveParametresParticuliersPlages(httpSession, parametresParticuliersDto);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	//check if creneau exists, if not, create it
	@Override
	public Creneau getCreneauByValues(HttpSession httpSession, String time1, String time2) throws ParseException {
		Creneau creneauToCheck = new Creneau();

		Time time1Converted = Time.valueOf(time1);
		Time time2Converted = Time.valueOf(time2);

		try {
			Creneau creneauxFind = apiCallsService.checkCreneauExists(httpSession, time1Converted, time2Converted);
			if(creneauxFind == null) {
				creneauToCheck.setCreneauDebut(time1Converted);
				creneauToCheck.setCreneauFin(time2Converted);
				return creneauToCheck;
			}
			return creneauxFind;
		} catch (ApiRequestErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

}
