package com.projetZare.auth.service.impl;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projetZare.auth.exceptions.ApiRequestErrorException;
import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.Parametres;
import com.projetZare.auth.model.ParametresDto;
import com.projetZare.auth.service.ParametresService;
import com.projetZare.auth.service.api.ApiCallsService;
import com.projetZare.auth.service.api.ApiCallsServiceParametres;

@Service
public class ParametresServiceImpl implements ParametresService{
	
	@Autowired
	ApiCallsServiceParametres apiCallsServiceParametres;
	
	@Autowired
	ApiCallsService apiCallsService;
	
	@Override
	public Parametres getParametres(HttpSession httpSession) {

		return apiCallsServiceParametres.parametresGenereauxAppli(httpSession);
	}
	
	@Override
	public HttpResponse<String> saveParametres(ParametresDto parametresDto, HttpSession httpSession) throws ParseException {
		
		//set Parametres with DTO
		Parametres parametresToSave = new Parametres(
				parametresDto.isOuvertLundiMidiG(), parametresDto.isOuvertLundiSoirG(), 
				parametresDto.isOuvertMardiMidiG(), parametresDto.isOuvertMardiSoirG(), 
				parametresDto.isOuvertMercrediMidiG(), parametresDto.isOuvertMercrediSoirG(),
				parametresDto.isOuvertJeudiMidiG(), parametresDto.isOuvertJeudiSoirG(),
				parametresDto.isOuvertVendrediMidiG(), parametresDto.isOuvertVendrediSoirG(),
				parametresDto.isOuvertSamediMidiG(), parametresDto.isOuvertSamediSoirG(),
				parametresDto.isOuvertDimancheMidiG(), parametresDto.isOuvertDimancheSoirG()
				);

		//this parameter is unique and will always be the first one in database
		parametresToSave.setIdParamG(1);
		
		//add all creneaux for this parameter
		parametresToSave.setCreneau1Midi(getCreneauByValues(httpSession, parametresDto.getCreneau1MidiDebut(), parametresDto.getCreneau1MidiFin()));
		parametresToSave.setCreneau1MidiNbResas(parametresDto.getCreneau1MidiNbResas());
		
		if(parametresDto.getCreneau2MidiDebut() != "" && parametresDto.getCreneau2MidiFin() != "") {
			parametresToSave.setCreneau2Midi(getCreneauByValues(httpSession, parametresDto.getCreneau2MidiDebut(), parametresDto.getCreneau2MidiFin()));
			parametresToSave.setCreneau2MidiNbResas(parametresDto.getCreneau2MidiNbResas());
		}else {
			parametresToSave.setCreneau2Midi(null);
		}
		
		if(parametresDto.getCreneau3MidiDebut() != "" && parametresDto.getCreneau3MidiFin() != "" ) {
			parametresToSave.setCreneau3Midi(getCreneauByValues(httpSession, parametresDto.getCreneau3MidiDebut(), parametresDto.getCreneau3MidiFin()));
			parametresToSave.setCreneau3MidiNbResas(parametresDto.getCreneau3MidiNbResas());
		}else {
			parametresToSave.setCreneau3Midi(null);
		}
		
		parametresToSave.setCreneau1Soir(getCreneauByValues(httpSession, parametresDto.getCreneau1SoirDebut(), parametresDto.getCreneau1SoirFin()));
		parametresToSave.setCreneau1SoirNbResas(parametresDto.getCreneau1SoirNbResas());
		
		if(parametresDto.getCreneau2SoirDebut() != "" && parametresDto.getCreneau2SoirFin() != "" ) {
			parametresToSave.setCreneau2Soir(getCreneauByValues(httpSession, parametresDto.getCreneau2SoirDebut(), parametresDto.getCreneau2SoirFin()));
			parametresToSave.setCreneau2SoirNbResas(parametresDto.getCreneau2SoirNbResas());
		}else {
			parametresToSave.setCreneau2Soir(null);
		}
		if(parametresDto.getCreneau3SoirDebut() != "" && parametresDto.getCreneau3SoirFin() != "" ) {
			parametresToSave.setCreneau3Soir(getCreneauByValues(httpSession, parametresDto.getCreneau3SoirDebut(), parametresDto.getCreneau3SoirFin()));
			parametresToSave.setCreneau3SoirNbResas(parametresDto.getCreneau3SoirNbResas());
		}else {
			parametresToSave.setCreneau3Soir(null);
		}
		
		
		try {
			return apiCallsServiceParametres.saveParametres(parametresToSave, httpSession);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	//check if creneau exists, if not, create it
	@Override
	public Creneau getCreneauByValues(HttpSession httpSession, String time1, String time2) throws ParseException {
		Creneau creneauToCheck = new Creneau();

		Time time1Converted = Time.valueOf(time1);
		Time time2Converted = Time.valueOf(time2);
		Creneau creneauxFind;
		try {
			creneauxFind = apiCallsService.checkCreneauExists(httpSession, time1Converted, time2Converted);
			if(creneauxFind == null) {
				creneauToCheck.setCreneauDebut(time1Converted);
				creneauToCheck.setCreneauFin(time2Converted);
				return creneauToCheck;
			}

			return creneauxFind;
		} catch (ApiRequestErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	@Override
	public boolean isOuvert(String result) {
		if(result == "on") {
			return true;
		}
		return false;
	}

	@Override
	public ParametresDto setParametresDto(HttpSession httpSession) {
		
		ParametresDto parametresDto = new ParametresDto();
		
		//Set values of parmetresDto form if parameters already set, for modification purpose
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		
		Parametres parametresSet = apiCallsServiceParametres.parametresGenereauxAppli(httpSession);
		
		//if parametres already set, fill parametresDto to prefill form		
		if(parametresSet != null) {
			parametresDto.setIdParamG(parametresSet.getIdParamG());
			
			//set all creneaux
			parametresDto.setCreneau1MidiDebut(dateFormat.format(parametresSet.getCreneau1Midi().getCreneauDebut()));
			parametresDto.setCreneau1MidiFin(dateFormat.format(parametresSet.getCreneau1Midi().getCreneauFin()));
			parametresDto.setCreneau1MidiNbResas(parametresSet.getCreneau1MidiNbResas());
			if(parametresSet.getCreneau2Midi() != null) {
				parametresDto.setCreneau2MidiDebut(dateFormat.format(parametresSet.getCreneau2Midi().getCreneauDebut()));
				parametresDto.setCreneau2MidiFin(dateFormat.format(parametresSet.getCreneau2Midi().getCreneauFin()));
				parametresDto.setCreneau2MidiNbResas(parametresSet.getCreneau2MidiNbResas());
			}
			if(parametresSet.getCreneau3Midi() != null) {
				parametresDto.setCreneau3MidiDebut(dateFormat.format(parametresSet.getCreneau3Midi().getCreneauDebut()));
				parametresDto.setCreneau3MidiFin(dateFormat.format(parametresSet.getCreneau3Midi().getCreneauFin()));
				parametresDto.setCreneau3MidiNbResas(parametresSet.getCreneau3MidiNbResas());
			}
			
			parametresDto.setCreneau1SoirDebut(dateFormat.format(parametresSet.getCreneau1Soir().getCreneauDebut()));
			parametresDto.setCreneau1SoirFin(dateFormat.format(parametresSet.getCreneau1Soir().getCreneauFin()));
			parametresDto.setCreneau1SoirNbResas(parametresSet.getCreneau1SoirNbResas());
			
			if(parametresSet.getCreneau2Soir() != null) {
				parametresDto.setCreneau2SoirDebut(dateFormat.format(parametresSet.getCreneau2Soir().getCreneauDebut()));
				parametresDto.setCreneau2SoirFin(dateFormat.format(parametresSet.getCreneau2Soir().getCreneauFin()));
				parametresDto.setCreneau2SoirNbResas(parametresSet.getCreneau2SoirNbResas());
			}
			if(parametresSet.getCreneau3Soir() != null) {
				parametresDto.setCreneau3SoirDebut(dateFormat.format(parametresSet.getCreneau3Soir().getCreneauDebut()));
				parametresDto.setCreneau3SoirFin(dateFormat.format(parametresSet.getCreneau3Soir().getCreneauFin()));
				parametresDto.setCreneau3SoirNbResas(parametresSet.getCreneau3SoirNbResas());
			}
			
			//set all opening days
			if(parametresSet.isOuvertLundiMidiG()) {
				parametresDto.setOuvertLundiMidiG(true);
			}
			if(parametresSet.isOuvertLundiSoirG()) {
				parametresDto.setOuvertLundiSoirG(true);
			}
			if(parametresSet.isOuvertMardiMidiG()) {
				parametresDto.setOuvertMardiMidiG(true);
			}
			if(parametresSet.isOuvertMardiSoirG()) {
				parametresDto.setOuvertMardiSoirG(true);
			}
			if(parametresSet.isOuvertMercrediMidiG()) {
				parametresDto.setOuvertMercrediMidiG(true);
			}
			if(parametresSet.isOuvertMercrediSoirG()) {
				parametresDto.setOuvertMercrediSoirG(true);
			}
			if(parametresSet.isOuvertJeudiMidiG()) {
				parametresDto.setOuvertJeudiMidiG(true);
			}
			if(parametresSet.isOuvertJeudiSoirG()) {
				parametresDto.setOuvertJeudiSoirG(true);
			}
			if(parametresSet.isOuvertVendrediMidiG()) {
				parametresDto.setOuvertVendrediMidiG(true);
			}
			if(parametresSet.isOuvertVendrediSoirG()) {
				parametresDto.setOuvertVendrediSoirG(true);
			}
			if(parametresSet.isOuvertSamediMidiG()) {
				parametresDto.setOuvertSamediMidiG(true);
			}
			if(parametresSet.isOuvertSamediSoirG()) {
				parametresDto.setOuvertSamediSoirG(true);
			}
			if(parametresSet.isOuvertDimancheMidiG()) {
				parametresDto.setOuvertDimancheMidiG(true);
			}
			if(parametresSet.isOuvertDimancheSoirG()) {
				parametresDto.setOuvertDimancheSoirG(true);
			}
		}
		return parametresDto;
	}
}
