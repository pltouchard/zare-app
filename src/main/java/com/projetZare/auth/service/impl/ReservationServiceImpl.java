package com.projetZare.auth.service.impl;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.Parametres;
import com.projetZare.auth.model.ParametresParticuliers;
import com.projetZare.auth.model.Reservation;
import com.projetZare.auth.service.ReservationService;
import com.projetZare.auth.service.api.ApiCallsService;
import com.projetZare.auth.service.api.ApiCallsServiceParametres;
import com.projetZare.auth.service.api.ApiCallsServiceParametresParticuliers;

@Service
public class ReservationServiceImpl implements ReservationService {

	@Autowired
	ApiCallsService apiCallsService;
	
	@Autowired
	ApiCallsServiceParametresParticuliers apiCallsServiceParametresParticuliers;
	
	@Autowired
	ApiCallsServiceParametres apiCallsServiceParametres;

	@Override
	public boolean ajoutReservation(Reservation validateReservation, HttpSession httpSession, int reservationIdPending) throws IOException, InterruptedException {
		boolean response = false;
		//get all reservations of the selected day
		ArrayList<Reservation> reservationsDay = apiCallsService.getAllReservationsByDate(validateReservation.getDateResa().toString(), httpSession);

		//get all parametres of the app
		Parametres parametresApp = apiCallsServiceParametres.parametresGenereauxAppli(httpSession);
		int nmbreCouvertsJour = 0;
		int nmbreCouvertsCreneau1Midi = 0;
		int nmbreCouvertsCreneau2Midi = 0;
		int nmbreCouvertsCreneau3Midi = 0;
		int nmbreCouvertsCreneau1Soir = 0;
		int nmbreCouvertsCreneau2Soir = 0;
		int nmbreCouvertsCreneau3Soir = 0;
		
		//increment all counters
		if(reservationsDay.size() > 0) {
			for(Reservation res : reservationsDay) {
				if(res.getIdResa() != reservationIdPending) {
					nmbreCouvertsJour += res.getNbCouvertsResa();
		
					if(res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Midi().getIdCreneau()) {
						nmbreCouvertsCreneau1Midi += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau2Midi() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Midi().getIdCreneau()) {
						nmbreCouvertsCreneau2Midi += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau3Midi() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Midi().getIdCreneau()) {
						nmbreCouvertsCreneau3Midi += res.getNbCouvertsResa();
					}else if(res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Soir().getIdCreneau()) {
						nmbreCouvertsCreneau1Soir += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau2Soir() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Soir().getIdCreneau()) {
						nmbreCouvertsCreneau2Soir += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau3Soir() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Soir().getIdCreneau()) {
						nmbreCouvertsCreneau3Soir += res.getNbCouvertsResa();
					}
				
				}
			}
		}


		//check if creneau is set for the selected creneau and if so, check if there's still enough seats available
		if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Midi().getIdCreneau() && (nmbreCouvertsCreneau1Midi + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau1MidiNbResas())) {
			response = apiCallAddResa(validateReservation, httpSession);
		}else if(parametresApp.getCreneau2Midi() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Midi().getIdCreneau() && (nmbreCouvertsCreneau2Midi + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau2MidiNbResas())) {
				response = apiCallAddResa(validateReservation, httpSession);
			}
		}else if(parametresApp.getCreneau3Midi() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Midi().getIdCreneau() && (nmbreCouvertsCreneau3Midi + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau3MidiNbResas())) {
				response = apiCallAddResa(validateReservation, httpSession);
			}
		}else if(parametresApp.getCreneau1Soir() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Soir().getIdCreneau() && (nmbreCouvertsCreneau1Soir + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau1SoirNbResas())) {
				response = apiCallAddResa(validateReservation, httpSession);
			}
		}else if(parametresApp.getCreneau2Soir() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Soir().getIdCreneau() && (nmbreCouvertsCreneau2Soir + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau2SoirNbResas())) {
				response = apiCallAddResa(validateReservation, httpSession);
			}
		}else if(parametresApp.getCreneau3Soir() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Soir().getIdCreneau() && (nmbreCouvertsCreneau3Soir + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau3SoirNbResas())) {
				response = apiCallAddResa(validateReservation, httpSession);
			}
		}
		return response;
	}
	
	private boolean apiCallAddResa(Reservation validateReservation, HttpSession httpSession) {
		HttpResponse<String> resp;
		try {
			resp = apiCallsService.ajouterResa(validateReservation, httpSession);
		
			if(resp.statusCode() != 201) {
				return false;
			}else {
				return true;
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public List<Creneau> allParametersCreneau(HttpSession httpSession, Parametres parametres, String dateParam){
		
		List<Creneau> tousLesCreneaux = new ArrayList<Creneau>();
		
		//set arrays to manage days of weeks with opening days
		boolean[] ouverturesMidi = new boolean[]{
				parametres.isOuvertLundiMidiG(),
				parametres.isOuvertMardiMidiG(),
				parametres.isOuvertMercrediMidiG(),
				parametres.isOuvertJeudiMidiG(),
				parametres.isOuvertVendrediMidiG(),
				parametres.isOuvertSamediMidiG(),
				parametres.isOuvertDimancheMidiG(),
				};
		boolean[] ouverturesSoir = new boolean[] {
				parametres.isOuvertLundiSoirG(),
				parametres.isOuvertMardiSoirG(),
				parametres.isOuvertMercrediSoirG(),
				parametres.isOuvertJeudiSoirG(),
				parametres.isOuvertVendrediSoirG(),
				parametres.isOuvertSamediSoirG(),
				parametres.isOuvertDimancheSoirG(),
		};
		
		//conversion string in date
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
		Calendar cal = Calendar.getInstance();
	    try {
			cal.setTime(formatter.parse(dateParam));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    int selectedDay = cal.get(Calendar.DAY_OF_WEEK);
		
	    
		List<ParametresParticuliers> parametreParticulier = apiCallsServiceParametresParticuliers.getParametresParticuliers(httpSession, dateParam, null);

		if(parametreParticulier.size() > 0) {
			
			ParametresParticuliers ppFind = parametreParticulier.get(0);
			
			if(ppFind.isOuvertMidiP()) {
				if(ppFind.getCreneau1Midi() != null) {
					tousLesCreneaux.add(ppFind.getCreneau1Midi());
				}
				if(ppFind.getCreneau2Midi() != null) {
					tousLesCreneaux.add(ppFind.getCreneau2Midi());
				}
				if(ppFind.getCreneau3Midi() != null) {
					tousLesCreneaux.add(ppFind.getCreneau3Midi());
				}
			}
			if(ppFind.isOuvertSoirP()) {
				if(ppFind.getCreneau1Soir() != null) {
					tousLesCreneaux.add(ppFind.getCreneau1Soir());
				}
				if(ppFind.getCreneau2Soir() != null) {
					tousLesCreneaux.add(ppFind.getCreneau2Soir());
				}
				if(ppFind.getCreneau3Soir() != null) {
					tousLesCreneaux.add(ppFind.getCreneau3Soir());
				}
			}
			
			return tousLesCreneaux;
		}
			
		
		
		tousLesCreneaux.add(parametres.getCreneau1Midi());
		if(parametres.getCreneau2Midi() != null) {
			tousLesCreneaux.add(parametres.getCreneau2Midi());
		}
		if(parametres.getCreneau3Midi() != null) {
			tousLesCreneaux.add(parametres.getCreneau3Midi());
		}
		
		tousLesCreneaux.add(parametres.getCreneau1Soir());
		if(parametres.getCreneau2Soir() != null) {
			tousLesCreneaux.add(parametres.getCreneau2Soir());
		}
		if(parametres.getCreneau3Soir() != null) {
			tousLesCreneaux.add(parametres.getCreneau3Soir());
		}
		Collections.sort(tousLesCreneaux, new Comparator<Creneau>() {
			@Override
			public int compare(Creneau c1, Creneau c2) {
				return c1.getCreneauDebut().compareTo(c2.getCreneauDebut());
			}
		});

		return tousLesCreneaux;
	}

}
