package com.projetZare.auth.service;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.Parametres;
import com.projetZare.auth.model.Reservation;

public interface ReservationService {

	public boolean ajoutReservation(Reservation validateReservation, HttpSession httpSession, int reservationIdPending) throws IOException, InterruptedException;
	public List<Creneau> allParametersCreneau(HttpSession httpSession, Parametres parametres, String dateParam);
	
}
