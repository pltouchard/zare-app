package com.projetZare.auth.service.api;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projetZare.auth.model.ParametresParticuliers;
import com.projetZare.auth.model.ParametresParticuliersDto;
import com.projetZare.auth.utils.Utils;

public class ApiCallsServiceParametresParticuliers {

	@Value("${api.call.url}")
	private String urlApi;
	
	@Autowired
	ApiCallsService apiCallsService;
	
	@Autowired
	Utils utils;
	
	/**
	 * 
	 * @param httpSession
	 * @param dateParamPDate1
	 * @param dateParamPDate2
	 * @return List of parameters particuliers within dates or for one date
	 */
	public List<ParametresParticuliers> getParametresParticuliers(HttpSession httpSession, String dateParamPDate1, String dateParamPDate2){
		String uri;
		if(dateParamPDate2 != "" && dateParamPDate2 != null) {
			 uri = "/parametres-particuliers?dateParamPDate1=" + dateParamPDate1 + "&dateParamPDate2=" + dateParamPDate2;
		}else {
			 uri = "/parametres-particuliers?dateParamPDate1=" + dateParamPDate1;
		}
		
		return utils.arrayResponseRequest(httpSession, uri, ParametresParticuliers.class);
	}
	
	
	/**
	 * 
	 * @param httpSession
	 * @param parametresParticuliersDto
	 * @return http code if fail/success adding parametres particuliers within 2 dates
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HttpResponse<String> saveParametresParticuliersPlages(HttpSession httpSession, ParametresParticuliersDto parametresParticuliersDto) throws IOException, InterruptedException{
		String uri = "/parametres-particuliers-plage";
		return utils.addRequest(parametresParticuliersDto, httpSession, uri);
	}
	
	/**
	 * 
	 * @param httpSession
	 * @param parametresParticuliersDto
	 * @return http code if fail/success adding parametres particuliers for multiple days
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HttpResponse<String> saveParametresParticuliersMulti(HttpSession httpSession, ParametresParticuliersDto parametresParticuliersDto) throws IOException, InterruptedException{
		String uri = "/parametres-particuliers-multi";
		return utils.addRequest(parametresParticuliersDto, httpSession, uri);
	}
	
}
