package com.projetZare.auth.service.api;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.projetZare.auth.exceptions.ApiRequestErrorException;
import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.Reservation;
import com.projetZare.auth.utils.Utils;

public class ApiCallsService {

	@Value("${api.call.url}")
	private String urlApi;
	
	@Autowired
	Utils utils;
	
	
	/**
	 * 
	 * @param httpSession
	 * @return tous les créneaux dans la BDD
	 */
	public ArrayList<Creneau> getAllCreneaux(HttpSession httpSession) {
		return utils.arrayResponseRequest(httpSession, "/creneaux", Creneau.class);
	}
	
	/**
	 * 
	 * @param httpSession
	 * @param date
	 * @return tous les créneaux par date et conditionné si ouvert ou pas le midi et le soir
	 */
	public ArrayList<Creneau> getAllCreneauxByDate(HttpSession httpSession, String date) {
		String uri = "/creneaux-date?date=" + date;
		return utils.arrayResponseRequest(httpSession, uri, Creneau.class);
	}
	
	/**
	 * 
	 * @param httpSession
	 * @param debut
	 * @param fin
	 * @return un créneau si il existe selon son heure de début et de fin
	 * @throws ApiRequestErrorException
	 */
	public Creneau checkCreneauExists(HttpSession httpSession, Time debut, Time fin) throws ApiRequestErrorException {
		List<Creneau> listeCreneauATrouver = new ArrayList<Creneau>();
		Creneau creneauATrouver = null;
		if(debut == null || fin == null) {
			throw new ApiRequestErrorException("Creneau.api.communication.error");
		}
		String uri = "/creneaux?heureDebut=" + debut + "&heureFin=" + fin;
		listeCreneauATrouver = utils.arrayResponseRequest(httpSession, uri, Creneau.class);
		if(!listeCreneauATrouver.isEmpty()) {
			creneauATrouver = listeCreneauATrouver.get(0);
		}
		
		return creneauATrouver;
	}
	
	
	/**
	 * 
	 * @param dateResa
	 * @param httpSession
	 * @return toutes les réservations par date
	 */
	public ArrayList<Reservation> getAllReservationsByDate(String dateResa, HttpSession httpSession) {
		String uri = "/reservations-date?dateResa="+dateResa;
		return utils.arrayResponseRequest(httpSession, uri, Reservation.class);
	}
	
	/**
	 * 
	 * @param dateDebut, dateFin
	 * @param httpSession
	 * @return toutes les réservations sur une plage de dates
	 */
	public ArrayList<Reservation> getAllReservationByPlageDates(String dateDebut, String dateFin, HttpSession httpSession){
		String uri="/reservations-plage-date?dateDebut=" + dateDebut + "&dateFin=" + dateFin;
		return utils.arrayResponseRequest(httpSession, uri, Reservation.class);
	}
	
	
	/**
	 * 
	 * @param idToGet
	 * @param httpSession
	 * @return une réservation selon son id
	 */
	public Reservation getReservationById(int idToGet,  HttpSession httpSession){
		String uri = "/reservations-id/?idResa=" + idToGet;
		return utils.singleResponseRequest(httpSession, uri, Reservation.class);
	}
	
	
	/**
	 * 
	 * @param httpSession
	 * @param id
	 * @return un créneau selon son id
	 */
	public Creneau getCreneauById(HttpSession httpSession, int id) {
		String uri = "/creneau/" + id;
		return utils.singleResponseRequest(httpSession, uri, Creneau.class);
	}
	
	/**
	 * 
	 * @param reservationAAjouter
	 * @param httpSession
	 * @return code de retour HTTP sur la requête d'ajout d'une réservation à l'API
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HttpResponse<String> ajouterResa(Reservation reservationAAjouter, HttpSession httpSession) throws IOException, InterruptedException {
		String uri = "/reservation-ajout";
		return utils.addRequest(reservationAAjouter, httpSession, uri);
	}
		
	
	/**
	 * 
	 * @param idASupprimer
	 * @param httpSession
	 * @return code retour http sur la requête de suppressionde réservation à l'API
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HttpResponse<String> supprimerResa(int idASupprimer, HttpSession httpSession) throws IOException, InterruptedException {
		String uri = "/reservation-suppression/"+idASupprimer;
		return utils.deleteRequest(idASupprimer, httpSession, uri);

	}
	
	
	}
