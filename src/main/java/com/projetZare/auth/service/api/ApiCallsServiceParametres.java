package com.projetZare.auth.service.api;

import java.io.IOException;
import java.net.http.HttpResponse;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.projetZare.auth.model.Parametres;
import com.projetZare.auth.utils.Utils;

public class ApiCallsServiceParametres {
	
	@Value("${api.call.url}")
	private String urlApi;
	
	@Autowired
	Utils utils;

	/**
	 * 
	 * @param httpSession
	 * @return all parameters from api
	 */
	public Parametres parametresGenereauxAppli(HttpSession httpSession) {
		String uri = "/parametres";
		return utils.singleResponseRequest(httpSession, uri, Parametres.class);
	}
	
	/**
	 * 
	 * @param parametres
	 * @param httpSession
	 * @return http code if fail/success adding parameters
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HttpResponse<String> saveParametres(Parametres parametres, HttpSession httpSession) throws IOException, InterruptedException{
		String uri =  "/parametres-ajout";
		return utils.addRequest(parametres, httpSession, uri);		
	}
}
