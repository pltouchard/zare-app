package com.projetZare.auth.service;

import java.net.http.HttpResponse;
import java.text.ParseException;

import javax.servlet.http.HttpSession;

import com.projetZare.auth.exceptions.ApiRequestErrorException;
import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.Parametres;
import com.projetZare.auth.model.ParametresDto;

public interface ParametresService {

	public Parametres getParametres(HttpSession httpSession);
	
	public HttpResponse<String> saveParametres(ParametresDto parametresDto, HttpSession httpSession) throws ParseException;
	
	public Creneau getCreneauByValues(HttpSession httpSession, String time1, String time2) throws ParseException;
	
	public boolean isOuvert(String result);
	
	public ParametresDto setParametresDto(HttpSession httpSession);
}
