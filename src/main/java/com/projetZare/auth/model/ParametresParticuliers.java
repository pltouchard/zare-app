package com.projetZare.auth.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ParametresParticuliers  implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private Date idParamP;
	private boolean ouvertMidiP;
	private boolean ouvertSoirP;
	private Creneau creneau1Midi;
	private int creneau1MidiNbResas;
	private Creneau creneau2Midi;
	private int creneau2MidiNbResas;
	private Creneau creneau3Midi;
	private int creneau3MidiNbResas;
	private Creneau creneau1Soir;
	private int creneau1SoirNbResas;
	private Creneau creneau2Soir;
	private int creneau2SoirNbResas;
	private Creneau creneau3Soir;
	private int creneau3SoirNbResas;

	public ParametresParticuliers() {
		super();
	}

	public ParametresParticuliers(Date idParamP, boolean ouvertMidiP, boolean ouvertSoirP, Creneau creneau1Midi,
			int creneau1MidiNbResas, Creneau creneau2Midi, int creneau2MidiNbResas, Creneau creneau3Midi,
			int creneau3MidiNbResas, Creneau creneau1Soir, int creneau1SoirNbResas, Creneau creneau2Soir,
			int creneau2SoirNbResas, Creneau creneau3Soir, int creneau3SoirNbResas) {
		super();
		this.idParamP = idParamP;
		this.ouvertMidiP = ouvertMidiP;
		this.ouvertSoirP = ouvertSoirP;
		this.creneau1Midi = creneau1Midi;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2Midi = creneau2Midi;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3Midi = creneau3Midi;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1Soir = creneau1Soir;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2Soir = creneau2Soir;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3Soir = creneau3Soir;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}
	
	

	public ParametresParticuliers(boolean ouvertMidiP, boolean ouvertSoirP, Creneau creneau1Midi, int creneau1MidiNbResas,
			Creneau creneau2Midi, int creneau2MidiNbResas, Creneau creneau3Midi, int creneau3MidiNbResas,
			Creneau creneau1Soir, int creneau1SoirNbResas, Creneau creneau2Soir, int creneau2SoirNbResas,
			Creneau creneau3Soir, int creneau3SoirNbResas) {
		super();
		this.ouvertMidiP = ouvertMidiP;
		this.ouvertSoirP = ouvertSoirP;
		this.creneau1Midi = creneau1Midi;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2Midi = creneau2Midi;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3Midi = creneau3Midi;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1Soir = creneau1Soir;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2Soir = creneau2Soir;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3Soir = creneau3Soir;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}

	public Date getIdParamP() {
		return idParamP;
	}

	public void setIdParamP(Date idParamP) {
		this.idParamP = idParamP;
	}

	

	public boolean isOuvertMidiP() {
		return ouvertMidiP;
	}

	public void setOuvertMidiP(boolean ouvertMidiP) {
		this.ouvertMidiP = ouvertMidiP;
	}

	public boolean isOuvertSoirP() {
		return ouvertSoirP;
	}

	public void setOuvertSoirP(boolean ouvertSoirP) {
		this.ouvertSoirP = ouvertSoirP;
	}

	public Creneau getCreneau1Midi() {
		return creneau1Midi;
	}

	public void setCreneau1Midi(Creneau creneau1Midi) {
		this.creneau1Midi = creneau1Midi;
	}

	public int getCreneau1MidiNbResas() {
		return creneau1MidiNbResas;
	}

	public void setCreneau1MidiNbResas(int creneau1MidiNbResas) {
		this.creneau1MidiNbResas = creneau1MidiNbResas;
	}

	public Creneau getCreneau2Midi() {
		return creneau2Midi;
	}

	public void setCreneau2Midi(Creneau creneau2Midi) {
		this.creneau2Midi = creneau2Midi;
	}

	public int getCreneau2MidiNbResas() {
		return creneau2MidiNbResas;
	}

	public void setCreneau2MidiNbResas(int creneau2MidiNbResas) {
		this.creneau2MidiNbResas = creneau2MidiNbResas;
	}

	public Creneau getCreneau3Midi() {
		return creneau3Midi;
	}

	public void setCreneau3Midi(Creneau creneau3Midi) {
		this.creneau3Midi = creneau3Midi;
	}

	public int getCreneau3MidiNbResas() {
		return creneau3MidiNbResas;
	}

	public void setCreneau3MidiNbResas(int creneau3MidiNbResas) {
		this.creneau3MidiNbResas = creneau3MidiNbResas;
	}

	public Creneau getCreneau1Soir() {
		return creneau1Soir;
	}

	public void setCreneau1Soir(Creneau creneau1Soir) {
		this.creneau1Soir = creneau1Soir;
	}

	public int getCreneau1SoirNbResas() {
		return creneau1SoirNbResas;
	}

	public void setCreneau1SoirNbResas(int creneau1SoirNbResas) {
		this.creneau1SoirNbResas = creneau1SoirNbResas;
	}

	public Creneau getCreneau2Soir() {
		return creneau2Soir;
	}

	public void setCreneau2Soir(Creneau creneau2Soir) {
		this.creneau2Soir = creneau2Soir;
	}

	public int getCreneau2SoirNbResas() {
		return creneau2SoirNbResas;
	}

	public void setCreneau2SoirNbResas(int creneau2SoirNbResas) {
		this.creneau2SoirNbResas = creneau2SoirNbResas;
	}

	public Creneau getCreneau3Soir() {
		return creneau3Soir;
	}

	public void setCreneau3Soir(Creneau creneau3Soir) {
		this.creneau3Soir = creneau3Soir;
	}

	public int getCreneau3SoirNbResas() {
		return creneau3SoirNbResas;
	}

	public void setCreneau3SoirNbResas(int creneau3SoirNbResas) {
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}

	@Override
	public String toString() {
		return "ParametresParticuliers [idParamP=" + idParamP + ", ouvertMidiP=" + ouvertMidiP + ", ouvertSoirP="
				+ ouvertSoirP + ", creneau1Midi=" + creneau1Midi + ", creneau1MidiNbResas=" + creneau1MidiNbResas
				+ ", creneau2Midi=" + creneau2Midi + ", creneau2MidiNbResas=" + creneau2MidiNbResas + ", creneau3Midi="
				+ creneau3Midi + ", creneau3MidiNbResas=" + creneau3MidiNbResas + ", creneau1Soir=" + creneau1Soir
				+ ", creneau1SoirNbResas=" + creneau1SoirNbResas + ", creneau2Soir=" + creneau2Soir
				+ ", creneau2SoirNbResas=" + creneau2SoirNbResas + ", creneau3Soir=" + creneau3Soir
				+ ", creneau3SoirNbResas=" + creneau3SoirNbResas + "]";
	}
	
	
}
