package com.projetZare.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties (value={"hibernateLazyInitializer", "handler", "fieldHandler"})
public class Parametres {

	private int idParamG;

	private Creneau creneau1Midi;
	
	private int creneau1MidiNbResas;

	private Creneau creneau2Midi;

	private int creneau2MidiNbResas;

	private Creneau creneau3Midi;

	private int creneau3MidiNbResas;

	private Creneau creneau1Soir;

	private int creneau1SoirNbResas;

	private Creneau creneau2Soir;

	private int creneau2SoirNbResas;

	private Creneau creneau3Soir;

	private int creneau3SoirNbResas;

	private boolean ouvertLundiMidiG;

	private boolean ouvertLundiSoirG;

	private boolean ouvertMardiMidiG;

	private boolean ouvertMardiSoirG;

	private boolean ouvertMercrediMidiG;

	private boolean ouvertMercrediSoirG;

	private boolean ouvertJeudiMidiG;

	private boolean ouvertJeudiSoirG;

	private boolean ouvertVendrediMidiG;

	private boolean ouvertVendrediSoirG;

	private boolean ouvertSamediMidiG;

	private boolean ouvertSamediSoirG;

	private boolean ouvertDimancheMidiG;

	private boolean ouvertDimancheSoirG;

	public Parametres() {
		super();
	}

	public Parametres(int idParamG, Creneau creneau1Midi,
			int creneau1MidiNbResas, Creneau creneau2Midi, int creneau2MidiNbResas, Creneau creneau3Midi,
			int creneau3MidiNbResas, Creneau creneau1Soir, int creneau1SoirNbResas, Creneau creneau2Soir,
			int creneau2SoirNbResas, Creneau creneau3Soir, int creneau3SoirNbResas, boolean ouvertLundiMidiG,
			boolean ouvertLundiSoirG, boolean ouvertMardiMidiG, boolean ouvertMardiSoirG, boolean ouvertMercrediMidiG,
			boolean ouvertMercrediSoirG, boolean ouvertJeudiMidiG, boolean ouvertJeudiSoirG,
			boolean ouvertVendrediMidiG, boolean ouvertVendrediSoirG, boolean ouvertSamediMidiG,
			boolean ouvertSamediSoirG, boolean ouvertDimancheMidiG, boolean ouvertDimancheSoirG) {
		super();
		this.idParamG = idParamG;
		this.creneau1Midi = creneau1Midi;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2Midi = creneau2Midi;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3Midi = creneau3Midi;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1Soir = creneau1Soir;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2Soir = creneau2Soir;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3Soir = creneau3Soir;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
		this.ouvertLundiMidiG = ouvertLundiMidiG;
		this.ouvertLundiSoirG = ouvertLundiSoirG;
		this.ouvertMardiMidiG = ouvertMardiMidiG;
		this.ouvertMardiSoirG = ouvertMardiSoirG;
		this.ouvertMercrediMidiG = ouvertMercrediMidiG;
		this.ouvertMercrediSoirG = ouvertMercrediSoirG;
		this.ouvertJeudiMidiG = ouvertJeudiMidiG;
		this.ouvertJeudiSoirG = ouvertJeudiSoirG;
		this.ouvertVendrediMidiG = ouvertVendrediMidiG;
		this.ouvertVendrediSoirG = ouvertVendrediSoirG;
		this.ouvertSamediMidiG = ouvertSamediMidiG;
		this.ouvertSamediSoirG = ouvertSamediSoirG;
		this.ouvertDimancheMidiG = ouvertDimancheMidiG;
		this.ouvertDimancheSoirG = ouvertDimancheSoirG;
	}
	
	

	public Parametres(boolean ouvertLundiMidiG, boolean ouvertLundiSoirG,
			boolean ouvertMardiMidiG, boolean ouvertMardiSoirG, boolean ouvertMercrediMidiG,
			boolean ouvertMercrediSoirG, boolean ouvertJeudiMidiG, boolean ouvertJeudiSoirG,
			boolean ouvertVendrediMidiG, boolean ouvertVendrediSoirG, boolean ouvertSamediMidiG,
			boolean ouvertSamediSoirG, boolean ouvertDimancheMidiG, boolean ouvertDimancheSoirG) {
		super();
		this.ouvertLundiMidiG = ouvertLundiMidiG;
		this.ouvertLundiSoirG = ouvertLundiSoirG;
		this.ouvertMardiMidiG = ouvertMardiMidiG;
		this.ouvertMardiSoirG = ouvertMardiSoirG;
		this.ouvertMercrediMidiG = ouvertMercrediMidiG;
		this.ouvertMercrediSoirG = ouvertMercrediSoirG;
		this.ouvertJeudiMidiG = ouvertJeudiMidiG;
		this.ouvertJeudiSoirG = ouvertJeudiSoirG;
		this.ouvertVendrediMidiG = ouvertVendrediMidiG;
		this.ouvertVendrediSoirG = ouvertVendrediSoirG;
		this.ouvertSamediMidiG = ouvertSamediMidiG;
		this.ouvertSamediSoirG = ouvertSamediSoirG;
		this.ouvertDimancheMidiG = ouvertDimancheMidiG;
		this.ouvertDimancheSoirG = ouvertDimancheSoirG;
	}

	public int getIdParamG() {
		return idParamG;
	}

	public void setIdParamG(int idParamG) {
		this.idParamG = idParamG;
	}

	public Creneau getCreneau1Midi() {
		return creneau1Midi;
	}

	public void setCreneau1Midi(Creneau creneau1Midi) {
		this.creneau1Midi = creneau1Midi;
	}

	public int getCreneau1MidiNbResas() {
		return creneau1MidiNbResas;
	}

	public void setCreneau1MidiNbResas(int creneau1MidiNbResas) {
		this.creneau1MidiNbResas = creneau1MidiNbResas;
	}

	public Creneau getCreneau2Midi() {
		return creneau2Midi;
	}

	public void setCreneau2Midi(Creneau creneau2Midi) {
		this.creneau2Midi = creneau2Midi;
	}

	public int getCreneau2MidiNbResas() {
		return creneau2MidiNbResas;
	}

	public void setCreneau2MidiNbResas(int creneau2MidiNbResas) {
		this.creneau2MidiNbResas = creneau2MidiNbResas;
	}

	public Creneau getCreneau3Midi() {
		return creneau3Midi;
	}

	public void setCreneau3Midi(Creneau creneau3Midi) {
		this.creneau3Midi = creneau3Midi;
	}

	public int getCreneau3MidiNbResas() {
		return creneau3MidiNbResas;
	}

	public void setCreneau3MidiNbResas(int creneau3MidiNbResas) {
		this.creneau3MidiNbResas = creneau3MidiNbResas;
	}

	public Creneau getCreneau1Soir() {
		return creneau1Soir;
	}

	public void setCreneau1Soir(Creneau creneau1Soir) {
		this.creneau1Soir = creneau1Soir;
	}

	public int getCreneau1SoirNbResas() {
		return creneau1SoirNbResas;
	}

	public void setCreneau1SoirNbResas(int creneau1SoirNbResas) {
		this.creneau1SoirNbResas = creneau1SoirNbResas;
	}

	public Creneau getCreneau2Soir() {
		return creneau2Soir;
	}

	public void setCreneau2Soir(Creneau creneau2Soir) {
		this.creneau2Soir = creneau2Soir;
	}

	public int getCreneau2SoirNbResas() {
		return creneau2SoirNbResas;
	}

	public void setCreneau2SoirNbResas(int creneau2SoirNbResas) {
		this.creneau2SoirNbResas = creneau2SoirNbResas;
	}

	public Creneau getCreneau3Soir() {
		return creneau3Soir;
	}

	public void setCreneau3Soir(Creneau creneau3Soir) {
		this.creneau3Soir = creneau3Soir;
	}

	public int getCreneau3SoirNbResas() {
		return creneau3SoirNbResas;
	}

	public void setCreneau3SoirNbResas(int creneau3SoirNbResas) {
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}

	public boolean isOuvertLundiMidiG() {
		return ouvertLundiMidiG;
	}

	public void setOuvertLundiMidiG(boolean ouvertLundiMidiG) {
		this.ouvertLundiMidiG = ouvertLundiMidiG;
	}

	public boolean isOuvertLundiSoirG() {
		return ouvertLundiSoirG;
	}

	public void setOuvertLundiSoirG(boolean ouvertLundiSoirG) {
		this.ouvertLundiSoirG = ouvertLundiSoirG;
	}

	public boolean isOuvertMardiMidiG() {
		return ouvertMardiMidiG;
	}

	public void setOuvertMardiMidiG(boolean ouvertMardiMidiG) {
		this.ouvertMardiMidiG = ouvertMardiMidiG;
	}

	public boolean isOuvertMardiSoirG() {
		return ouvertMardiSoirG;
	}

	public void setOuvertMardiSoirG(boolean ouvertMardiSoirG) {
		this.ouvertMardiSoirG = ouvertMardiSoirG;
	}

	public boolean isOuvertMercrediMidiG() {
		return ouvertMercrediMidiG;
	}

	public void setOuvertMercrediMidiG(boolean ouvertMercrediMidiG) {
		this.ouvertMercrediMidiG = ouvertMercrediMidiG;
	}

	public boolean isOuvertMercrediSoirG() {
		return ouvertMercrediSoirG;
	}

	public void setOuvertMercrediSoirG(boolean ouvertMercrediSoirG) {
		this.ouvertMercrediSoirG = ouvertMercrediSoirG;
	}

	public boolean isOuvertJeudiMidiG() {
		return ouvertJeudiMidiG;
	}

	public void setOuvertJeudiMidiG(boolean ouvertJeudiMidiG) {
		this.ouvertJeudiMidiG = ouvertJeudiMidiG;
	}

	public boolean isOuvertJeudiSoirG() {
		return ouvertJeudiSoirG;
	}

	public void setOuvertJeudiSoirG(boolean ouvertJeudiSoirG) {
		this.ouvertJeudiSoirG = ouvertJeudiSoirG;
	}

	public boolean isOuvertVendrediMidiG() {
		return ouvertVendrediMidiG;
	}

	public void setOuvertVendrediMidiG(boolean ouvertVendrediMidiG) {
		this.ouvertVendrediMidiG = ouvertVendrediMidiG;
	}

	public boolean isOuvertVendrediSoirG() {
		return ouvertVendrediSoirG;
	}

	public void setOuvertVendrediSoirG(boolean ouvertVendrediSoirG) {
		this.ouvertVendrediSoirG = ouvertVendrediSoirG;
	}

	public boolean isOuvertSamediMidiG() {
		return ouvertSamediMidiG;
	}

	public void setOuvertSamediMidiG(boolean ouvertSamediMidiG) {
		this.ouvertSamediMidiG = ouvertSamediMidiG;
	}

	public boolean isOuvertSamediSoirG() {
		return ouvertSamediSoirG;
	}

	public void setOuvertSamediSoirG(boolean ouvertSamediSoirG) {
		this.ouvertSamediSoirG = ouvertSamediSoirG;
	}

	public boolean isOuvertDimancheMidiG() {
		return ouvertDimancheMidiG;
	}

	public void setOuvertDimancheMidiG(boolean ouvertDimancheMidiG) {
		this.ouvertDimancheMidiG = ouvertDimancheMidiG;
	}

	public boolean isOuvertDimancheSoirG() {
		return ouvertDimancheSoirG;
	}

	public void setOuvertDimancheSoirG(boolean ouvertDimancheSoirG) {
		this.ouvertDimancheSoirG = ouvertDimancheSoirG;
	}

	@Override
	public String toString() {
		return "Parametres [idParamG=" + idParamG+ ", creneau1Midi=" + creneau1Midi + ", creneau1MidiNbResas=" + creneau1MidiNbResas
				+ ", creneau2Midi=" + creneau2Midi + ", creneau2MidiNbResas=" + creneau2MidiNbResas + ", creneau3Midi="
				+ creneau3Midi + ", creneau3MidiNbResas=" + creneau3MidiNbResas + ", creneau1Soir=" + creneau1Soir
				+ ", creneau1SoirNbResas=" + creneau1SoirNbResas + ", creneau2Soir=" + creneau2Soir
				+ ", creneau2SoirNbResas=" + creneau2SoirNbResas + ", creneau3Soir=" + creneau3Soir
				+ ", creneau3SoirNbResas=" + creneau3SoirNbResas + ", ouvertLundiMidiG=" + ouvertLundiMidiG
				+ ", ouvertLundiSoirG=" + ouvertLundiSoirG + ", ouvertMardiMidiG=" + ouvertMardiMidiG
				+ ", ouvertMardiSoirG=" + ouvertMardiSoirG + ", ouvertMercrediMidiG=" + ouvertMercrediMidiG
				+ ", ouvertMercrediSoirG=" + ouvertMercrediSoirG + ", ouvertJeudiMidiG=" + ouvertJeudiMidiG
				+ ", ouvertJeudiSoirG=" + ouvertJeudiSoirG + ", ouvertVendrediMidiG=" + ouvertVendrediMidiG
				+ ", ouvertVendrediSoirG=" + ouvertVendrediSoirG + ", ouvertSamediMidiG=" + ouvertSamediMidiG
				+ ", ouvertSamediSoirG=" + ouvertSamediSoirG + ", ouvertDimancheMidiG=" + ouvertDimancheMidiG
				+ ", ouvertDimancheSoirG=" + ouvertDimancheSoirG + "]";
	}
	
	
}
