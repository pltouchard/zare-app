package com.projetZare.auth.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "application_infos")
public class ApplicationInfos {

	@Id
	private int id;
	
	@Column(name = "param_generaux_set")
	private boolean paramGenerauxSet;
	
	@Column(name = "date_set_param_generaux")
	private Date setParamGeneraux;

	public ApplicationInfos() {
		super();
	}


	public ApplicationInfos(int id, boolean paramGenerauxSet, Date setParamGeneraux) {
		super();
		this.id = id;
		this.paramGenerauxSet = paramGenerauxSet;
		this.setParamGeneraux = setParamGeneraux;
	}
	

	public ApplicationInfos(boolean paramGenerauxSet, Date setParamGeneraux) {
		super();
		this.paramGenerauxSet = paramGenerauxSet;
		this.setParamGeneraux = setParamGeneraux;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isParamGenerauxSet() {
		return paramGenerauxSet;
	}

	public void setParamGenerauxSet(boolean paramGenerauxSet) {
		this.paramGenerauxSet = paramGenerauxSet;
	}

	public Date getSetParamGeneraux() {
		return setParamGeneraux;
	}

	public void setSetParamGeneraux(Date setParamGeneraux) {
		this.setParamGeneraux = setParamGeneraux;
	}
	
	
	
}
