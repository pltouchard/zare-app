package com.projetZare.auth.model;

import java.util.Arrays;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ParametreParticulierDto {
	
	private String dates;
	private boolean multi;
	private boolean ouvertMidiP;
	private boolean ouvertSoirP;

	private String creneau1MidiDebut;
	private String creneau1MidiFin;
	private Integer creneau1MidiNbResas;
	
	private String creneau2MidiDebut;
	private String creneau2MidiFin;	
	private Integer creneau2MidiNbResas;
	
	private String creneau3MidiDebut;
	private String creneau3MidiFin;
	private Integer creneau3MidiNbResas;
	

	private String creneau1SoirDebut;
	private String creneau1SoirFin;
	private Integer creneau1SoirNbResas;
	
	private String creneau2SoirDebut;
	private String creneau2SoirFin;
	private Integer creneau2SoirNbResas;
	
	private String creneau3SoirDebut;
	private String creneau3SoirFin;
	private Integer creneau3SoirNbResas;
	
	
	public ParametreParticulierDto() {
		super();
	}


	


	public ParametreParticulierDto(boolean multi, boolean ouvertMidiP, boolean ouvertSoirP,
			String creneau1MidiDebut, String creneau1MidiFin,Integer creneau1MidiNbResas,
			String creneau2MidiDebut, String creneau2MidiFin, Integer creneau2MidiNbResas, String creneau3MidiDebut,
			String creneau3MidiFin, Integer creneau3MidiNbResas, String creneau1SoirDebut, String creneau1SoirFin, Integer creneau1SoirNbResas,
			String creneau2SoirDebut, String creneau2SoirFin, Integer creneau2SoirNbResas, String creneau3SoirDebut,
			String creneau3SoirFin, Integer creneau3SoirNbResas) {
		super();
		this.multi = multi;
		this.ouvertMidiP = ouvertMidiP;
		this.ouvertSoirP = ouvertSoirP;
		this.creneau1MidiDebut = creneau1MidiDebut;
		this.creneau1MidiFin = creneau1MidiFin;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2MidiDebut = creneau2MidiDebut;
		this.creneau2MidiFin = creneau2MidiFin;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3MidiDebut = creneau3MidiDebut;
		this.creneau3MidiFin = creneau3MidiFin;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1SoirDebut = creneau1SoirDebut;
		this.creneau1SoirFin = creneau1SoirFin;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2SoirDebut = creneau2SoirDebut;
		this.creneau2SoirFin = creneau2SoirFin;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3SoirDebut = creneau3SoirDebut;
		this.creneau3SoirFin = creneau3SoirFin;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}





	public ParametreParticulierDto(String dates, boolean multi, boolean ouvertMidiP, boolean ouvertSoirP,
			String creneau1MidiDebut, String creneau1MidiFin, Integer creneau1MidiNbResas,
			String creneau2MidiDebut, String creneau2MidiFin, Integer creneau2MidiNbResas, String creneau3MidiDebut,
			String creneau3MidiFin, Integer creneau3MidiNbResas, String creneau1SoirDebut, String creneau1SoirFin, Integer creneau1SoirNbResas,
			String creneau2SoirDebut, String creneau2SoirFin, Integer creneau2SoirNbResas, String creneau3SoirDebut,
			String creneau3SoirFin, Integer creneau3SoirNbResas) {
		super();
		this.dates = dates;
		this.multi = multi;
		this.ouvertMidiP = ouvertMidiP;
		this.ouvertSoirP = ouvertSoirP;
		this.creneau1MidiDebut = creneau1MidiDebut;
		this.creneau1MidiFin = creneau1MidiFin;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2MidiDebut = creneau2MidiDebut;
		this.creneau2MidiFin = creneau2MidiFin;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3MidiDebut = creneau3MidiDebut;
		this.creneau3MidiFin = creneau3MidiFin;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1SoirDebut = creneau1SoirDebut;
		this.creneau1SoirFin = creneau1SoirFin;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2SoirDebut = creneau2SoirDebut;
		this.creneau2SoirFin = creneau2SoirFin;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3SoirDebut = creneau3SoirDebut;
		this.creneau3SoirFin = creneau3SoirFin;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}




	

	public boolean isMulti() {
		return multi;
	}


	public void setMulti(boolean multi) {
		this.multi = multi;
	}



	public String getDates() {
		return dates;
	}


	public void setDates(String dates) {
		this.dates = dates;
	}


	public boolean getOuvertMidiP() {
		return ouvertMidiP;
	}


	public void setOuvertMidiP(boolean ouvertMidiP) {
		this.ouvertMidiP = ouvertMidiP;
	}


	public boolean getOuvertSoirP() {
		return ouvertSoirP;
	}


	public void setOuvertSoirP(boolean ouvertSoirP) {
		this.ouvertSoirP = ouvertSoirP;
	}


	public String getCreneau1MidiDebut() {
		return creneau1MidiDebut;
	}


	public void setCreneau1MidiDebut(String creneau1MidiDebut) {
		this.creneau1MidiDebut = creneau1MidiDebut;
	}


	public String getCreneau1MidiFin() {
		return creneau1MidiFin;
	}


	public void setCreneau1MidiFin(String creneau1MidiFin) {
		this.creneau1MidiFin = creneau1MidiFin;
	}


	public Integer getCreneau1MidiNbResas() {
		return creneau1MidiNbResas;
	}


	public void setCreneau1MidiNbResas(Integer creneau1MidiNbResas) {
		this.creneau1MidiNbResas = creneau1MidiNbResas;
	}


	public String getCreneau2MidiDebut() {
		return creneau2MidiDebut;
	}


	public void setCreneau2MidiDebut(String creneau2MidiDebut) {
		this.creneau2MidiDebut = creneau2MidiDebut;
	}


	public String getCreneau2MidiFin() {
		return creneau2MidiFin;
	}


	public void setCreneau2MidiFin(String creneau2MidiFin) {
		this.creneau2MidiFin = creneau2MidiFin;
	}


	public Integer getCreneau2MidiNbResas() {
		return creneau2MidiNbResas;
	}


	public void setCreneau2MidiNbResas(Integer creneau2MidiNbResas) {
		this.creneau2MidiNbResas = creneau2MidiNbResas;
	}


	public String getCreneau3MidiDebut() {
		return creneau3MidiDebut;
	}


	public void setCreneau3MidiDebut(String creneau3MidiDebut) {
		this.creneau3MidiDebut = creneau3MidiDebut;
	}


	public String getCreneau3MidiFin() {
		return creneau3MidiFin;
	}


	public void setCreneau3MidiFin(String creneau3MidiFin) {
		this.creneau3MidiFin = creneau3MidiFin;
	}


	public Integer getCreneau3MidiNbResas() {
		return creneau3MidiNbResas;
	}


	public void setCreneau3MidiNbResas(Integer creneau3MidiNbResas) {
		this.creneau3MidiNbResas = creneau3MidiNbResas;
	}


	public String getCreneau1SoirDebut() {
		return creneau1SoirDebut;
	}


	public void setCreneau1SoirDebut(String creneau1SoirDebut) {
		this.creneau1SoirDebut = creneau1SoirDebut;
	}


	public String getCreneau1SoirFin() {
		return creneau1SoirFin;
	}


	public void setCreneau1SoirFin(String creneau1SoirFin) {
		this.creneau1SoirFin = creneau1SoirFin;
	}


	public Integer getCreneau1SoirNbResas() {
		return creneau1SoirNbResas;
	}


	public void setCreneau1SoirNbResas(Integer creneau1SoirNbResas) {
		this.creneau1SoirNbResas = creneau1SoirNbResas;
	}


	public String getCreneau2SoirDebut() {
		return creneau2SoirDebut;
	}


	public void setCreneau2SoirDebut(String creneau2SoirDebut) {
		this.creneau2SoirDebut = creneau2SoirDebut;
	}


	public String getCreneau2SoirFin() {
		return creneau2SoirFin;
	}


	public void setCreneau2SoirFin(String creneau2SoirFin) {
		this.creneau2SoirFin = creneau2SoirFin;
	}


	public Integer getCreneau2SoirNbResas() {
		return creneau2SoirNbResas;
	}


	public void setCreneau2SoirNbResas(Integer creneau2SoirNbResas) {
		this.creneau2SoirNbResas = creneau2SoirNbResas;
	}


	public String getCreneau3SoirDebut() {
		return creneau3SoirDebut;
	}


	public void setCreneau3SoirDebut(String creneau3SoirDebut) {
		this.creneau3SoirDebut = creneau3SoirDebut;
	}


	public String getCreneau3SoirFin() {
		return creneau3SoirFin;
	}


	public void setCreneau3SoirFin(String creneau3SoirFin) {
		this.creneau3SoirFin = creneau3SoirFin;
	}


	public Integer getCreneau3SoirNbResas() {
		return creneau3SoirNbResas;
	}


	public void setCreneau3SoirNbResas(Integer creneau3SoirNbResas) {
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}


	@Override
	public String toString() {
		return "ParametreParticulierDto [dates=" + dates + ", multi=" + multi + ", ouvertMidiP=" + ouvertMidiP
				+ ", ouvertSoirP=" + ouvertSoirP + ", creneau1MidiDebut=" + creneau1MidiDebut + ", creneau1MidiFin="
				+ creneau1MidiFin + ", creneau1MidiNbResas=" + creneau1MidiNbResas + ", creneau2MidiDebut="
				+ creneau2MidiDebut + ", creneau2MidiFin=" + creneau2MidiFin + ", creneau2MidiNbResas="
				+ creneau2MidiNbResas + ", creneau3MidiDebut=" + creneau3MidiDebut + ", creneau3MidiFin="
				+ creneau3MidiFin + ", creneau3MidiNbResas=" + creneau3MidiNbResas + ", creneau1SoirDebut="
				+ creneau1SoirDebut + ", creneau1SoirFin=" + creneau1SoirFin + ", creneau1SoirNbResas="
				+ creneau1SoirNbResas + ", creneau2SoirDebut=" + creneau2SoirDebut + ", creneau2SoirFin="
				+ creneau2SoirFin + ", creneau2SoirNbResas=" + creneau2SoirNbResas + ", creneau3SoirDebut="
				+ creneau3SoirDebut + ", creneau3SoirFin=" + creneau3SoirFin + ", creneau3SoirNbResas="
				+ creneau3SoirNbResas + "]";
	}
	
	
}
