package com.projetZare.auth.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "creneau")
public class Creneau {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idCreneau;
	
	@Column(name = "creneau_debut")
	private Time creneauDebut;
	
	@Column(name = "creneau_fin")
	private Time creneauFin;
	
	

	public Creneau(int idCreneau, Time creneauDebut, Time creneauFin) {
		super();
		this.idCreneau = idCreneau;
		this.creneauDebut = creneauDebut;
		this.creneauFin = creneauFin;
	}

	public Creneau(Time creneauDebut, Time creneauFin) {
		super();
		this.creneauDebut = creneauDebut;
		this.creneauFin = creneauFin;
	}

	public Creneau() {
		super();
	}

	public int getIdCreneau() {
		return idCreneau;
	}

	public Time getCreneauDebut() {
		return creneauDebut;
	}

	public void setCreneauDebut(Time creneauDebut) {
		this.creneauDebut = creneauDebut;
	}

	public Time getCreneauFin() {
		return creneauFin;
	}

	public void setCreneauFin(Time creneauFin) {
		this.creneauFin = creneauFin;
	}

	@Override
	public String toString() {
		return "Creneau [idCreneau=" + idCreneau + ", creneauDebut=" + creneauDebut + ", creneauFin=" + creneauFin
				+ "]";
	}

	
	
	
}
