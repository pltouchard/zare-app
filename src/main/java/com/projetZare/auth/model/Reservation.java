package com.projetZare.auth.model;

import java.sql.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Reservation {
	
	private int idResa;

	@Temporal(TemporalType.DATE)
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd",timezone="Europe/Paris")
	private Date dateResa;

	@Size(min = 3, max = 100, message="{Taille.reservation.nom}")
	@NotEmpty(message = "{NotEmpty}")
	private String nomResa;
	
	@NotEmpty(message = "{NotEmpty}")
	@Size(min = 10, max = 15, message="{Taille.reservation.telephone}")
	private String telephoneResa;

	@Size(min = 6, max = 80, message="{Taille.reservation.mail}")
	@NotEmpty(message = "{NotEmpty}")
	private String mailResa;

	@Min(value=1, message = "{Taille.reservation.nombreCouverts}")
	@Max(value=15, message = "{Taille.reservation.nombreCouverts}")
	private int nbCouvertsResa;

	private Creneau creneauResa;

	private String serviceResa;

	private boolean showResa;
	
	private String notesResa;
	
	private boolean notesResaImportant;
	
	private boolean habitue;

	public Reservation() {
		super();
	}




	public Reservation(Date dateResa, @NotEmpty(message = "{NotEmpty}") String nomResa,
			@NotEmpty(message = "{NotEmpty}") @Size(min = 10, max = 15, message = "{Taille.reservation.telephone}") String telephoneResa,
			@Size(min = 6, max = 80, message = "{Taille.reservation.mail}") @NotEmpty(message = "{NotEmpty}") String mailResa,
			@Min(value = 1, message = "{Taille.reservation.nombreCouverts}") @Max(value = 15, message = "{Taille.reservation.nombreCouverts}") int nbCouvertsResa,
			Creneau creneauResa, String serviceResa, boolean showResa, String notesResa, boolean notesResaImportant, boolean habitue) {
		super();
		this.dateResa = dateResa;
		this.nomResa = nomResa;
		this.telephoneResa = telephoneResa;
		this.mailResa = mailResa;
		this.nbCouvertsResa = nbCouvertsResa;
		this.creneauResa = creneauResa;
		this.serviceResa = serviceResa;
		this.showResa = showResa;
		this.notesResa = notesResa;
		this.notesResaImportant = notesResaImportant;
		this.habitue = habitue;
	}







	public Reservation(int idResa, Date dateResa, @NotEmpty(message = "{NotEmpty}") String nomResa,
			@NotEmpty(message = "{NotEmpty}") @Size(min = 10, max = 15, message = "{Taille.reservation.telephone}") String telephoneResa,
			@Size(min = 6, max = 80, message = "{Taille.reservation.mail}") @NotEmpty(message = "{NotEmpty}") String mailResa,
			@Min(value = 1, message = "{Taille.reservation.nombreCouverts}") @Max(value = 15, message = "{Taille.reservation.nombreCouverts}") int nbCouvertsResa,
			Creneau creneauResa, String serviceResa, boolean showResa, String notesResa, boolean notesResaImportant, boolean habitue) {
		super();
		this.idResa = idResa;
		this.dateResa = dateResa;
		this.nomResa = nomResa;
		this.telephoneResa = telephoneResa;
		this.mailResa = mailResa;
		this.nbCouvertsResa = nbCouvertsResa;
		this.creneauResa = creneauResa;
		this.serviceResa = serviceResa;
		this.showResa = showResa;
		this.notesResa = notesResa;
		this.notesResaImportant = notesResaImportant;
		this.habitue = habitue;
	}







	public int getIdResa() {
		return idResa;
	}


	public void setIdResa(int idResa) {
		this.idResa = idResa;
	}



	public Date getDateResa() {
		return dateResa;
	}



	public void setDateResa(Date dateResa) {
		this.dateResa = dateResa;
	}



	public String getNomResa() {
		return nomResa;
	}



	public void setNomResa(String nomResa) {
		this.nomResa = nomResa;
	}



	public String getTelephoneResa() {
		return telephoneResa;
	}



	public void setTelephoneResa(String telephoneResa) {
		this.telephoneResa = telephoneResa;
	}



	public String getMailResa() {
		return mailResa;
	}



	public void setMailResa(String mailResa) {
		this.mailResa = mailResa;
	}



	public int getNbCouvertsResa() {
		return nbCouvertsResa;
	}



	public void setNbCouvertsResa(int nbCouvertsResa) {
		this.nbCouvertsResa = nbCouvertsResa;
	}



	public Creneau getCreneauResa() {
		return creneauResa;
	}



	public void setCreneauResa(Creneau creneauResa) {
		this.creneauResa = creneauResa;
	}



	public String getServiceResa() {
		return serviceResa;
	}



	public void setServiceResa(String serviceResa) {
		this.serviceResa = serviceResa;
	}



	public boolean isShowResa() {
		return showResa;
	}



	public void setShowResa(boolean showResa) {
		this.showResa = showResa;
	}


	public String getNotesResa() {
		return notesResa;
	}

	public void setNotesResa(String notesResa) {
		this.notesResa = notesResa;
	}




	public boolean isNotesResaImportant() {
		return notesResaImportant;
	}




	public void setNotesResaImportant(boolean notesResaImportant) {
		this.notesResaImportant = notesResaImportant;
	}




	public boolean isHabitue() {
		return habitue;
	}




	public void setHabitue(boolean habitue) {
		this.habitue = habitue;
	}




	@Override
	public String toString() {
		return "Reservation [idResa=" + idResa + ", dateResa=" + dateResa + ", nomResa=" + nomResa + ", telephoneResa="
				+ telephoneResa + ", mailResa=" + mailResa + ", nbCouvertsResa=" + nbCouvertsResa + ", creneauResa="
				+ creneauResa + ", serviceResa=" + serviceResa + ", showResa=" + showResa + ", notesResa=" + notesResa
				+ ", notesResaImportant=" + notesResaImportant + ", habitue=" + habitue + "]";
	}


	


	
	
	
}
