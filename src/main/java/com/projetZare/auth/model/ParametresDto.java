package com.projetZare.auth.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ParametresDto {
	
	private int idParamG;
	
	@NotBlank(message="Un début de service minimum obligatoire")
	private String creneau1MidiDebut;
	@NotBlank(message="Une fin de service minimum obligatoire")
	private String creneau1MidiFin;
	@Min(value=1, message="Minimum 1 réservation pour le nombre de couverts maximum du 1er service du midi")
	private Integer creneau1MidiNbResas;
	
	private String creneau2MidiDebut;
	private String creneau2MidiFin;
	
	private Integer creneau2MidiNbResas;
	
	private String creneau3MidiDebut;
	private String creneau3MidiFin;
	private Integer creneau3MidiNbResas;
	
	@NotBlank(message="Un début de service minimum obligatoire")
	private String creneau1SoirDebut;
	@NotBlank(message="Une fin de service minimum obligatoire")
	private String creneau1SoirFin;
	@Min(value=1, message="Minimum 1 réservation pour le nombre de couverts maximum du 1er service du soir")
	private Integer creneau1SoirNbResas;
	
	private String creneau2SoirDebut;
	private String creneau2SoirFin;
	private Integer creneau2SoirNbResas;
	
	private String creneau3SoirDebut;
	private String creneau3SoirFin;
	private Integer creneau3SoirNbResas;
	
	private boolean ouvertLundiMidiG;
	
	private boolean ouvertLundiSoirG;
	
	private boolean ouvertMardiMidiG;
	
	private boolean ouvertMardiSoirG;
	
	private boolean ouvertMercrediMidiG;
	
	private boolean ouvertMercrediSoirG;
	
	private boolean ouvertJeudiMidiG;
	
	private boolean ouvertJeudiSoirG;
	
	private boolean ouvertVendrediMidiG;
	
	private boolean ouvertVendrediSoirG;
	
	private boolean ouvertSamediMidiG;
	
	private boolean ouvertSamediSoirG;
	
	private boolean ouvertDimancheMidiG;
	
	private boolean ouvertDimancheSoirG;

	public ParametresDto() {
		super();
	}

	public ParametresDto(int idParamG,
			String creneau1MidiDebut, String creneau1MidiFin, int creneau1MidiNbResas,
			String creneau2MidiDebut, String creneau2MidiFin, int creneau2MidiNbResas, String creneau3MidiDebut,
			String creneau3MidiFin, int creneau3MidiNbResas, String creneau1SoirDebut, String creneau1SoirFin,
			int creneau1SoirNbResas, String creneau2SoirDebut, String creneau2SoirFin, int creneau2SoirNbResas,
			String creneau3SoirDebut, String creneau3SoirFin, int creneau3SoirNbResas, boolean ouvertLundiMidiG,
			boolean ouvertLundiSoirG, boolean ouvertMardiMidiG, boolean ouvertMardiSoirG, boolean ouvertMercrediMidiG,
			boolean ouvertMercrediSoirG, boolean ouvertJeudiMidiG, boolean ouvertJeudiSoirG,
			boolean ouvertVendrediMidiG, boolean ouvertVendrediSoirG, boolean ouvertSamediMidiG,
			boolean ouvertSamediSoirG, boolean ouvertDimancheMidiG, boolean ouvertDimancheSoirG) {
		super();
		this.idParamG = idParamG;
		this.creneau1MidiDebut = creneau1MidiDebut;
		this.creneau1MidiFin = creneau1MidiFin;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2MidiDebut = creneau2MidiDebut;
		this.creneau2MidiFin = creneau2MidiFin;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3MidiDebut = creneau3MidiDebut;
		this.creneau3MidiFin = creneau3MidiFin;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1SoirDebut = creneau1SoirDebut;
		this.creneau1SoirFin = creneau1SoirFin;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2SoirDebut = creneau2SoirDebut;
		this.creneau2SoirFin = creneau2SoirFin;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3SoirDebut = creneau3SoirDebut;
		this.creneau3SoirFin = creneau3SoirFin;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
		this.ouvertLundiMidiG = ouvertLundiMidiG;
		this.ouvertLundiSoirG = ouvertLundiSoirG;
		this.ouvertMardiMidiG = ouvertMardiMidiG;
		this.ouvertMardiSoirG = ouvertMardiSoirG;
		this.ouvertMercrediMidiG = ouvertMercrediMidiG;
		this.ouvertMercrediSoirG = ouvertMercrediSoirG;
		this.ouvertJeudiMidiG = ouvertJeudiMidiG;
		this.ouvertJeudiSoirG = ouvertJeudiSoirG;
		this.ouvertVendrediMidiG = ouvertVendrediMidiG;
		this.ouvertVendrediSoirG = ouvertVendrediSoirG;
		this.ouvertSamediMidiG = ouvertSamediMidiG;
		this.ouvertSamediSoirG = ouvertSamediSoirG;
		this.ouvertDimancheMidiG = ouvertDimancheMidiG;
		this.ouvertDimancheSoirG = ouvertDimancheSoirG;
	}

	public ParametresDto(
			String creneau1MidiDebut, String creneau1MidiFin, int creneau1MidiNbResas,
			String creneau2MidiDebut, String creneau2MidiFin, int creneau2MidiNbResas, String creneau3MidiDebut,
			String creneau3MidiFin, int creneau3MidiNbResas, String creneau1SoirDebut, String creneau1SoirFin,
			int creneau1SoirNbResas, String creneau2SoirDebut, String creneau2SoirFin, int creneau2SoirNbResas,
			String creneau3SoirDebut, String creneau3SoirFin, int creneau3SoirNbResas, boolean ouvertLundiMidiG,
			boolean ouvertLundiSoirG, boolean ouvertMardiMidiG, boolean ouvertMardiSoirG, boolean ouvertMercrediMidiG,
			boolean ouvertMercrediSoirG, boolean ouvertJeudiMidiG, boolean ouvertJeudiSoirG,
			boolean ouvertVendrediMidiG, boolean ouvertVendrediSoirG, boolean ouvertSamediMidiG,
			boolean ouvertSamediSoirG, boolean ouvertDimancheMidiG, boolean ouvertDimancheSoirG) {
		super();
		this.creneau1MidiDebut = creneau1MidiDebut;
		this.creneau1MidiFin = creneau1MidiFin;
		this.creneau1MidiNbResas = creneau1MidiNbResas;
		this.creneau2MidiDebut = creneau2MidiDebut;
		this.creneau2MidiFin = creneau2MidiFin;
		this.creneau2MidiNbResas = creneau2MidiNbResas;
		this.creneau3MidiDebut = creneau3MidiDebut;
		this.creneau3MidiFin = creneau3MidiFin;
		this.creneau3MidiNbResas = creneau3MidiNbResas;
		this.creneau1SoirDebut = creneau1SoirDebut;
		this.creneau1SoirFin = creneau1SoirFin;
		this.creneau1SoirNbResas = creneau1SoirNbResas;
		this.creneau2SoirDebut = creneau2SoirDebut;
		this.creneau2SoirFin = creneau2SoirFin;
		this.creneau2SoirNbResas = creneau2SoirNbResas;
		this.creneau3SoirDebut = creneau3SoirDebut;
		this.creneau3SoirFin = creneau3SoirFin;
		this.creneau3SoirNbResas = creneau3SoirNbResas;
		this.ouvertLundiMidiG = ouvertLundiMidiG;
		this.ouvertLundiSoirG = ouvertLundiSoirG;
		this.ouvertMardiMidiG = ouvertMardiMidiG;
		this.ouvertMardiSoirG = ouvertMardiSoirG;
		this.ouvertMercrediMidiG = ouvertMercrediMidiG;
		this.ouvertMercrediSoirG = ouvertMercrediSoirG;
		this.ouvertJeudiMidiG = ouvertJeudiMidiG;
		this.ouvertJeudiSoirG = ouvertJeudiSoirG;
		this.ouvertVendrediMidiG = ouvertVendrediMidiG;
		this.ouvertVendrediSoirG = ouvertVendrediSoirG;
		this.ouvertSamediMidiG = ouvertSamediMidiG;
		this.ouvertSamediSoirG = ouvertSamediSoirG;
		this.ouvertDimancheMidiG = ouvertDimancheMidiG;
		this.ouvertDimancheSoirG = ouvertDimancheSoirG;
	}

	public int getIdParamG() {
		return idParamG;
	}

	public void setIdParamG(int idParamG) {
		this.idParamG = idParamG;
	}

	public String getCreneau1MidiDebut() {
		return creneau1MidiDebut;
	}

	public void setCreneau1MidiDebut(String creneau1MidiDebut) {
		this.creneau1MidiDebut = creneau1MidiDebut;
	}

	public String getCreneau1MidiFin() {
		return creneau1MidiFin;
	}

	public void setCreneau1MidiFin(String creneau1MidiFin) {
		this.creneau1MidiFin = creneau1MidiFin;
	}

	public Integer getCreneau1MidiNbResas() {
		return creneau1MidiNbResas;
	}

	public void setCreneau1MidiNbResas(Integer creneau1MidiNbResas) {
		this.creneau1MidiNbResas = creneau1MidiNbResas;
	}

	public String getCreneau2MidiDebut() {
		return creneau2MidiDebut;
	}

	public void setCreneau2MidiDebut(String creneau2MidiDebut) {
		this.creneau2MidiDebut = creneau2MidiDebut;
	}

	public String getCreneau2MidiFin() {
		return creneau2MidiFin;
	}

	public void setCreneau2MidiFin(String creneau2MidiFin) {
		this.creneau2MidiFin = creneau2MidiFin;
	}

	public Integer getCreneau2MidiNbResas() {
		return creneau2MidiNbResas;
	}

	public void setCreneau2MidiNbResas(Integer creneau2MidiNbResas) {
		this.creneau2MidiNbResas = creneau2MidiNbResas;
	}

	public String getCreneau3MidiDebut() {
		return creneau3MidiDebut;
	}

	public void setCreneau3MidiDebut(String creneau3MidiDebut) {
		this.creneau3MidiDebut = creneau3MidiDebut;
	}

	public String getCreneau3MidiFin() {
		return creneau3MidiFin;
	}

	public void setCreneau3MidiFin(String creneau3MidiFin) {
		this.creneau3MidiFin = creneau3MidiFin;
	}

	public Integer getCreneau3MidiNbResas() {
		return creneau3MidiNbResas;
	}

	public void setCreneau3MidiNbResas(Integer creneau3MidiNbResas) {
		this.creneau3MidiNbResas = creneau3MidiNbResas;
	}

	public String getCreneau1SoirDebut() {
		return creneau1SoirDebut;
	}

	public void setCreneau1SoirDebut(String creneau1SoirDebut) {
		this.creneau1SoirDebut = creneau1SoirDebut;
	}

	public String getCreneau1SoirFin() {
		return creneau1SoirFin;
	}

	public void setCreneau1SoirFin(String creneau1SoirFin) {
		this.creneau1SoirFin = creneau1SoirFin;
	}

	public Integer getCreneau1SoirNbResas() {
		return creneau1SoirNbResas;
	}

	public void setCreneau1SoirNbResas(Integer creneau1SoirNbResas) {
		this.creneau1SoirNbResas = creneau1SoirNbResas;
	}

	public String getCreneau2SoirDebut() {
		return creneau2SoirDebut;
	}

	public void setCreneau2SoirDebut(String creneau2SoirDebut) {
		this.creneau2SoirDebut = creneau2SoirDebut;
	}

	public String getCreneau2SoirFin() {
		return creneau2SoirFin;
	}

	public void setCreneau2SoirFin(String creneau2SoirFin) {
		this.creneau2SoirFin = creneau2SoirFin;
	}

	public Integer getCreneau2SoirNbResas() {
		return creneau2SoirNbResas;
	}

	public void setCreneau2SoirNbResas(Integer creneau2SoirNbResas) {
		this.creneau2SoirNbResas = creneau2SoirNbResas;
	}

	public String getCreneau3SoirDebut() {
		return creneau3SoirDebut;
	}

	public void setCreneau3SoirDebut(String creneau3SoirDebut) {
		this.creneau3SoirDebut = creneau3SoirDebut;
	}

	public String getCreneau3SoirFin() {
		return creneau3SoirFin;
	}

	public void setCreneau3SoirFin(String creneau3SoirFin) {
		this.creneau3SoirFin = creneau3SoirFin;
	}

	public Integer getCreneau3SoirNbResas() {
		return creneau3SoirNbResas;
	}

	public void setCreneau3SoirNbResas(Integer creneau3SoirNbResas) {
		this.creneau3SoirNbResas = creneau3SoirNbResas;
	}

	public boolean isOuvertLundiMidiG() {
		return ouvertLundiMidiG;
	}

	public void setOuvertLundiMidiG(boolean ouvertLundiMidiG) {
		this.ouvertLundiMidiG = ouvertLundiMidiG;
	}

	public boolean isOuvertLundiSoirG() {
		return ouvertLundiSoirG;
	}

	public void setOuvertLundiSoirG(boolean ouvertLundiSoirG) {
		this.ouvertLundiSoirG = ouvertLundiSoirG;
	}

	public boolean isOuvertMardiMidiG() {
		return ouvertMardiMidiG;
	}

	public void setOuvertMardiMidiG(boolean ouvertMardiMidiG) {
		this.ouvertMardiMidiG = ouvertMardiMidiG;
	}

	public boolean isOuvertMardiSoirG() {
		return ouvertMardiSoirG;
	}

	public void setOuvertMardiSoirG(boolean ouvertMardiSoirG) {
		this.ouvertMardiSoirG = ouvertMardiSoirG;
	}

	public boolean isOuvertMercrediMidiG() {
		return ouvertMercrediMidiG;
	}

	public void setOuvertMercrediMidiG(boolean ouvertMercrediMidiG) {
		this.ouvertMercrediMidiG = ouvertMercrediMidiG;
	}

	public boolean isOuvertMercrediSoirG() {
		return ouvertMercrediSoirG;
	}

	public void setOuvertMercrediSoirG(boolean ouvertMercrediSoirG) {
		this.ouvertMercrediSoirG = ouvertMercrediSoirG;
	}

	public boolean isOuvertJeudiMidiG() {
		return ouvertJeudiMidiG;
	}

	public void setOuvertJeudiMidiG(boolean ouvertJeudiMidiG) {
		this.ouvertJeudiMidiG = ouvertJeudiMidiG;
	}

	public boolean isOuvertJeudiSoirG() {
		return ouvertJeudiSoirG;
	}

	public void setOuvertJeudiSoirG(boolean ouvertJeudiSoirG) {
		this.ouvertJeudiSoirG = ouvertJeudiSoirG;
	}

	public boolean isOuvertVendrediMidiG() {
		return ouvertVendrediMidiG;
	}

	public void setOuvertVendrediMidiG(boolean ouvertVendrediMidiG) {
		this.ouvertVendrediMidiG = ouvertVendrediMidiG;
	}

	public boolean isOuvertVendrediSoirG() {
		return ouvertVendrediSoirG;
	}

	public void setOuvertVendrediSoirG(boolean ouvertVendrediSoirG) {
		this.ouvertVendrediSoirG = ouvertVendrediSoirG;
	}

	public boolean isOuvertSamediMidiG() {
		return ouvertSamediMidiG;
	}

	public void setOuvertSamediMidiG(boolean ouvertSamediMidiG) {
		this.ouvertSamediMidiG = ouvertSamediMidiG;
	}

	public boolean isOuvertSamediSoirG() {
		return ouvertSamediSoirG;
	}

	public void setOuvertSamediSoirG(boolean ouvertSamediSoirG) {
		this.ouvertSamediSoirG = ouvertSamediSoirG;
	}

	public boolean isOuvertDimancheMidiG() {
		return ouvertDimancheMidiG;
	}

	public void setOuvertDimancheMidiG(boolean ouvertDimancheMidiG) {
		this.ouvertDimancheMidiG = ouvertDimancheMidiG;
	}

	public boolean isOuvertDimancheSoirG() {
		return ouvertDimancheSoirG;
	}

	public void setOuvertDimancheSoirG(boolean ouvertDimancheSoirG) {
		this.ouvertDimancheSoirG = ouvertDimancheSoirG;
	}

	@Override
	public String toString() {
		return "ParametresDto [idParamG=" + idParamG+ ", creneau1MidiDebut=" + creneau1MidiDebut + ", creneau1MidiFin=" + creneau1MidiFin
				+ ", creneau1MidiNbResas=" + creneau1MidiNbResas + ", creneau2MidiDebut=" + creneau2MidiDebut
				+ ", creneau2MidiFin=" + creneau2MidiFin + ", creneau2MidiNbResas=" + creneau2MidiNbResas
				+ ", creneau3MidiDebut=" + creneau3MidiDebut + ", creneau3MidiFin=" + creneau3MidiFin
				+ ", creneau3MidiNbResas=" + creneau3MidiNbResas + ", creneau1SoirDebut=" + creneau1SoirDebut
				+ ", creneau1SoirFin=" + creneau1SoirFin + ", creneau1SoirNbResas=" + creneau1SoirNbResas
				+ ", creneau2SoirDebut=" + creneau2SoirDebut + ", creneau2SoirFin=" + creneau2SoirFin
				+ ", creneau2SoirNbResas=" + creneau2SoirNbResas + ", creneau3SoirDebut=" + creneau3SoirDebut
				+ ", creneau3SoirFin=" + creneau3SoirFin + ", creneau3SoirNbResas=" + creneau3SoirNbResas
				+ ", ouvertLundiMidiG=" + ouvertLundiMidiG + ", ouvertLundiSoirG=" + ouvertLundiSoirG
				+ ", ouvertMardiMidiG=" + ouvertMardiMidiG + ", ouvertMardiSoirG=" + ouvertMardiSoirG
				+ ", ouvertMercrediMidiG=" + ouvertMercrediMidiG + ", ouvertMercrediSoirG=" + ouvertMercrediSoirG
				+ ", ouvertJeudiMidiG=" + ouvertJeudiMidiG + ", ouvertJeudiSoirG=" + ouvertJeudiSoirG
				+ ", ouvertVendrediMidiG=" + ouvertVendrediMidiG + ", ouvertVendrediSoirG=" + ouvertVendrediSoirG
				+ ", ouvertSamediMidiG=" + ouvertSamediMidiG + ", ouvertSamediSoirG=" + ouvertSamediSoirG
				+ ", ouvertDimancheMidiG=" + ouvertDimancheMidiG + ", ouvertDimancheSoirG=" + ouvertDimancheSoirG + "]";
	}

	


	
	
	
	
}
