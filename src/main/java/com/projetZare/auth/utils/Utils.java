package com.projetZare.auth.utils;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projetZare.auth.service.impl.SecurityServiceImpl;

public class Utils {

	@Autowired
	SecurityServiceImpl securityServiceImpl;
	
	@Value("${api.call.url}")
	private String urlApi;
	
	/**
	 * Vérifier si le token renvoyé par l'API existe
	 * @param httpSession
	 * @param response
	 * @throws IOException
	 */
	public void checkToken(HttpSession httpSession, HttpServletResponse response) throws IOException {
		Object token = httpSession.getAttribute("jwtToken");
		 try {
			 if(token == null) {
				 String getToken = securityServiceImpl.call_me();
				 httpSession.setAttribute("jwtToken", getToken);
			 }
			 		
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	((HttpServletResponse)response).sendRedirect("/login?logout=deconnection");
	    }
	}
	
	/**
	 * 
	 * @param <T>
	 * @param httpSession
	 * @param uri
	 * @param classe
	 * @return un objet après une requête à l'API
	 */
	public <T> T singleResponseRequest(HttpSession httpSession, String uri, Class<T> classe){
		String token = (String) httpSession.getAttribute("jwtToken");
		String authToken = "Bearer " + token;
		T element = null;
		
		String requestBody = "";
		HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(urlApi + uri))
                .GET()
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", authToken)
                .build();

        try {
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			 ObjectMapper om = new ObjectMapper();
			 
			 JsonNode data = om.readTree(response.body());
			 String jsonData = om.writeValueAsString(data);
			 element = om.readValue(jsonData,classe);
			 
			 return element;
			 
        }catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return null;
	}

	/**
	 * 
	 * @param <T>
	 * @param httpSession
	 * @param uri
	 * @param classe
	 * @return un tableau d'objets après une requête à l'API
	 */
	public <T> ArrayList<T> arrayResponseRequest(HttpSession httpSession, String uri, Class<T> classe ) {
		String token = (String) httpSession.getAttribute("jwtToken");
		String authToken = "Bearer " + token;
		ArrayList<T> tableau = new ArrayList<T>();
		
		String requestBody = "";
		HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(urlApi + uri))
                .GET()
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", authToken)
                .build();

        try {
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			 ObjectMapper om = new ObjectMapper();
			 for(JsonNode data : om.readTree(response.body())) {
				 String jsonData = om.writeValueAsString(data);
				 T element = om.readValue(jsonData,classe);
				 tableau.add(element);
			 }
			 
			 return tableau;
			 
        }catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return null;
	}	
	
	/**
	 * 
	 * @param <T>
	 * @param elementAAJouter
	 * @param httpSession
	 * @param uri
	 * @return un code retour HTTP en fonction du résultat de la requête d'ajout à l'API
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public <T> HttpResponse<String> addRequest(T elementAAJouter, HttpSession httpSession, String uri) throws IOException, InterruptedException{
		String token = (String) httpSession.getAttribute("jwtToken");
		String authToken = "Bearer " + token;
		ObjectMapper om = new ObjectMapper();

		String requestBody = om.writeValueAsString(elementAAJouter);

		HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(urlApi + uri))
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", authToken)
                .build();
        return client.send(request, HttpResponse.BodyHandlers.ofString());
	}
	
	/**
	 * 
	 * @param idASupprimer
	 * @param httpSession
	 * @param uri
	 * @return un code retour HTTP en fonction du résultat de la requête de suppression à l'API
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HttpResponse<String> deleteRequest(int idASupprimer, HttpSession httpSession, String uri) throws IOException, InterruptedException{
		String token = (String) httpSession.getAttribute("jwtToken");
		String authToken = "Bearer " + token;

		HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(urlApi + uri))
                .DELETE()
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", authToken)
                .build();
        
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        
        return response;
	}
}
