package com.projetZare.auth.controllers;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.projetZare.auth.model.Creneau;
import com.projetZare.auth.model.Reservation;
import com.projetZare.auth.model.ReservationDto;
import com.projetZare.auth.service.api.ApiCallsService;
import com.projetZare.auth.service.api.ApiCallsServiceParametres;
import com.projetZare.auth.service.impl.ReservationServiceImpl;

@Controller
public class ReservationController {
	
	@Autowired
	ApiCallsService apiCallsService;
	
	@Autowired
	ApiCallsServiceParametres apiCallsServiceParametres;

	@Autowired
	ReservationServiceImpl reservationServiceImpl;
	
	@Value("${api.call.url}")
	private String urlApi;
	
	
	@GetMapping({"/reservation"})
	public String reservation(Model model, HttpSession httpSession,  @ModelAttribute("reservationDto") ReservationDto reservationFormulaire, @RequestParam(value="idResa", required = false) Integer idResa) {
		Object token = httpSession.getAttribute("jwtToken");
		
		//set correct date format
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String parametresDateToCheck;
		
		Reservation reservationToGet = null;
		if(idResa == null) {
			idResa = 0;
		}
		
		if(idResa > 0) {
			reservationToGet = apiCallsService.getReservationById(idResa, httpSession);
			reservationFormulaire = new ReservationDto(reservationToGet.getIdResa(), reservationToGet.getDateResa(), reservationToGet.getNomResa(), reservationToGet.getTelephoneResa(), reservationToGet.getMailResa(), reservationToGet.getNbCouvertsResa(), reservationToGet.getCreneauResa().getIdCreneau(), reservationToGet.getServiceResa(), reservationToGet.isShowResa(), reservationToGet.getNotesResa(), reservationToGet.isNotesResaImportant(), reservationToGet.isHabitue());
			model.addAttribute("idCreneau", reservationToGet.getCreneauResa().getIdCreneau());
			model.addAttribute("reservationDto", reservationFormulaire);
		}
		
		//if request come from incorrect form submit -> get selected date, if not get current day
		if(model.getAttribute("messageErreurFormulaire") != null){
			 parametresDateToCheck = String.valueOf(reservationFormulaire.getDateResa());
		}else if(idResa > 0){
			parametresDateToCheck = String.valueOf(reservationToGet.getDateResa());
		}else {
			LocalDateTime now = LocalDateTime.now();
			parametresDateToCheck = dtf.format(now);
		}
		
		model.addAttribute("nomPage", "Réserver");
		model.addAttribute("userToken", token);
		
		List<Creneau> tousLesCreneaux = apiCallsService.getAllCreneauxByDate(httpSession, parametresDateToCheck);
		model.addAttribute("tousLesCreneaux", tousLesCreneaux);
		
		
		
		return "ajout-reservation";	
	}
	
	
	//on reservation form validation -> send to api to save or update
	@PostMapping({"/reservation-ajout"})
	public String reservationAjout(@Value("${app.config.limiteSoir}") String limite, RedirectAttributes redirectAttributes, @Valid @ModelAttribute("reservationDto") ReservationDto reservationAAjouter, BindingResult result, Model model, HttpSession httpSession, HttpServletRequest request) throws IOException, InterruptedException, ParseException, BindException {

		Reservation reservationOk;
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
		String stringToTime = limite;
		Date limiteCreneau = sdf.parse(stringToTime);
		HttpResponse<String> resp;

		RedirectView redirect = new RedirectView(request.getContextPath() + "/reservation");
		redirect.setExposeModelAttributes(false);
		
		if (result.hasErrors()) {
		    	redirectAttributes.addFlashAttribute("messageErreurFormulaire", "Une erreur s'est produite ! Réservation non ajoutée");
		    	redirectAttributes.addFlashAttribute("reservationDto", reservationAAjouter);
		    	redirectAttributes.addFlashAttribute("errors", result.getAllErrors());
		    	return "redirect:/reservation";

	    }else {
	    	Creneau creneauReservationOk = apiCallsService.getCreneauById(httpSession, reservationAAjouter.getCreneauResaId());

	    	if(creneauReservationOk.getCreneauFin().after(limiteCreneau)){
				reservationAAjouter.setServiceResa("soir");
			}else {
				reservationAAjouter.setServiceResa("midi");
			}
	    	reservationOk = new Reservation(reservationAAjouter.getIdResa(), reservationAAjouter.getDateResa(), reservationAAjouter.getNomResa(), reservationAAjouter.getTelephoneResa(), reservationAAjouter.getMailResa(), reservationAAjouter.getNbCouvertsResa(), creneauReservationOk, reservationAAjouter.getServiceResa(), true, reservationAAjouter.getNotesResa(), reservationAAjouter.isNotesResaImportant(), reservationAAjouter.isHabitue());
	    }


		resp = apiCallsService.ajouterResa(reservationOk, httpSession);
		
		//redirections after request complete
		if(reservationOk.getIdResa() != 0 && resp.statusCode() == 201) {
			redirect = new RedirectView(request.getContextPath() + "/reservations-consulter");
			redirect.setExposeModelAttributes(false);
			redirectAttributes.addFlashAttribute("messageAjoutFormulaire", "Réservation modifiée");
		}else if(reservationOk.getIdResa() == 0 && resp.statusCode() == 201){
			redirectAttributes.addFlashAttribute("messageAjoutFormulaire", "Réservation ajoutée");
		}else {
			redirectAttributes.addFlashAttribute("messageErreurFormulaire", resp.body().toString());
	    	redirectAttributes.addFlashAttribute("reservationDto", reservationAAjouter);
	    	return "redirect:/reservation";
		}

		return "redirect:/reservation";
	}
	
	//delete booking on API
	@PostMapping({"/reservation-suppression"})
	public RedirectView supprimerReservation(RedirectAttributes redirectAttributes, @RequestParam("idsuppr") int idASupprimer, ModelMap model, HttpSession httpSession, HttpServletRequest request) throws IOException, InterruptedException {
		RedirectView redirect = new RedirectView(request.getContextPath() + "/reservations-consulter");
		redirect.setExposeModelAttributes(false);
		
		HttpResponse<String> resultSuppr = apiCallsService.supprimerResa(idASupprimer, httpSession);
		
		if(resultSuppr.statusCode() == 200) {
			redirectAttributes.addFlashAttribute("messageAjoutFormulaire", "Réservation supprimée");
			return redirect;
		}else {
			redirectAttributes.addFlashAttribute("messageErreurFormulaire", "Réservation non trouvée. Aucune suppression");
			return redirect;
		}
		
		
	}
	
	//Get all bookings for a day
	@GetMapping({"/reservations-consulter"})
	public String consulterReservations(Model model, HttpSession httpSession, @RequestParam(value="dateResa", required = false) Date dateResa) {
		Object token = httpSession.getAttribute("jwtToken");
		
		if(token != null) {
			model.addAttribute("userToken", token.toString());
		}else {
			return "redirect:logout";
		}
		
		model.addAttribute("nomPage", "Toutes les réservations");
		return "allReservations";
	}
}
