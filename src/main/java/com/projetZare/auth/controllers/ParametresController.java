package com.projetZare.auth.controllers;

import java.net.http.HttpResponse;
import java.text.ParseException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.projetZare.auth.exceptions.ApiRequestErrorException;
import com.projetZare.auth.model.ParametresDto;
import com.projetZare.auth.service.impl.ParametresServiceImpl;
import com.projetZare.auth.validator.ParametresValidator;

@Controller
public class ParametresController {
	
	@Autowired
	ParametresServiceImpl parametresService;
	
	@Autowired
    private MessageSource messageSource;
	
	@Autowired
	private ParametresValidator parametresValidator;

	 

	@GetMapping({"/set-params"})
	public String getParametresSetter(HttpServletRequest request, Model model, @ModelAttribute ParametresDto parametresDto, HttpSession httpSession) throws IllegalArgumentException, IllegalAccessException {
      
		parametresDto = parametresService.setParametresDto(httpSession);

		if(parametresDto.getIdParamG() == 1) {
			model.addAttribute("parametresDto", parametresDto);
		}
		model.addAttribute("nomPage", "Paramètres Généraux");
		model.addAttribute("erreurFormulaireParametres", request.getParameter("erreurFormulaireParametres"));
		model.addAttribute("validationFormulaireParametres", request.getParameter("validationFormulaireParametres"));

		return "parametres-premiere";
	}
	
	@PostMapping({"/set-params"})
	public String postParametresGetter(HttpServletRequest request, 
			Model model,
			@ModelAttribute @Valid ParametresDto parametresDto, 
			BindingResult result, 
			final RedirectAttributes redirectAttributes, 
			HttpSession httpSession) {
		
		parametresValidator.validate(parametresDto, result);
		if(result.hasErrors()) {	
			model.addAttribute("erreurFormulaireParametres", messageSource.getMessage("Parametres.formulaire.erreur", null, Locale.FRANCE));
			return "parametres-premiere";
		}
		
		
		try {
			
			HttpResponse<String> resp = parametresService.saveParametres(parametresDto, httpSession);
			if(resp.statusCode() != 201) {
				throw new ApiRequestErrorException("Erreur d'ajout des paramètres particuliers");
			}
			redirectAttributes.addAttribute("validationFormulaireParametres", "Paramètres correctement modifiés" );
		} catch (ParseException e) {
			redirectAttributes.addAttribute("erreurFormulaireParametres", messageSource.getMessage("Parametres.formulaire.erreur", null, Locale.FRANCE));
			e.printStackTrace();
			return "redirect:/set-params";
		}catch (ApiRequestErrorException e) {
			redirectAttributes.addAttribute("erreurFormulaireParametres", messageSource.getMessage("Parametres.formulaire.erreur", null, Locale.FRANCE));
			e.printStackTrace();
			return "redirect:/set-params";
		}
		return "redirect:/set-params";
		
	}
}
