package com.projetZare.auth.controllers;

import java.net.http.HttpResponse;
import java.text.ParseException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.projetZare.auth.exceptions.ApiRequestErrorException;
import com.projetZare.auth.model.ParametreParticulierDto;
import com.projetZare.auth.service.impl.ParametreParticulierServiceImpl;
import com.projetZare.auth.validator.ParametreParticulierValidator;

@Controller
public class ParametresParticuliersController {
	
	@Autowired
	ParametreParticulierServiceImpl parametreParticulierServiceImpl;
	
	@Autowired
	ParametreParticulierValidator parametresParticuliersValidator;
	
	@Autowired
    private MessageSource messageSource;

	@GetMapping({"/parametres-particuliers"})
	public String getParametresParticuliers(HttpServletRequest request, Model model, @ModelAttribute ParametreParticulierDto parametreParticulierDto, HttpSession httpSession) {
		Object token = httpSession.getAttribute("jwtToken");
		
		model.addAttribute("nomPage", "Paramètres Particuliers");
		model.addAttribute("erreurFormulaireParametres", request.getParameter("erreurFormulaireParametres"));
		model.addAttribute("validationFormulaireParametres", request.getParameter("validationFormulaireParametres"));
		model.addAttribute("userToken", token);
		
		return "parametres-particulier";
	}
	
	@PostMapping({"/parametres-particuliers"})
	public String saveParametreParticulier(
			HttpServletRequest request, 
			Model model,
			@ModelAttribute @Valid ParametreParticulierDto parametreParticulierDto, 
			BindingResult result, 
			final RedirectAttributes redirectAttributes, 
			HttpSession httpSession) throws ParseException {
		
		parametresParticuliersValidator.validate(parametreParticulierDto, result);
		
		if(result.hasErrors()) {
			model.addAttribute("nomPage", "Paramètres Particuliers");
			model.addAttribute("erreurFormulaireParametres", messageSource.getMessage("Parametres.formulaire.erreur", null, Locale.FRANCE));
			model.addAttribute("validationPpFormulaire", "false");
			model.addAttribute("isRange", parametreParticulierDto.isMulti());
			return "parametres-particulier";
		}
		
		try {
			HttpResponse<String> resp = parametreParticulierServiceImpl.saveParametresPaticuliers(parametreParticulierDto, httpSession);
			if(resp.statusCode() != 200) {
				throw new ApiRequestErrorException("Erreur d'ajout des paramètres particuliers");
			}
			redirectAttributes.addAttribute("validationFormulaireParametres", "Paramètres correctement modifiés" );
		} catch (ParseException e) {
			redirectAttributes.addAttribute("erreurFormulaireParametres", messageSource.getMessage("Parametres.formulaire.erreur", null, Locale.FRANCE));
			e.printStackTrace();
			return "redirect:/parametres-particuliers";
		} catch (ApiRequestErrorException e) {
			redirectAttributes.addAttribute("erreurFormulaireParametres", messageSource.getMessage("Parametres.formulaire.erreur", null, Locale.FRANCE));
			e.printStackTrace();
			return "redirect:/parametres-particuliers";
		}
		
		return "redirect:/parametres-particuliers";
	}
	
}
