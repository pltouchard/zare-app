package com.projetZare.auth.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErreurController  implements ErrorController{
	
	@GetMapping("/error")
	public String renderErrorPage(HttpServletRequest httpRequest, Model model) {

        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);
 
        switch (httpErrorCode) {
            case 400: {
                errorMsg = "Erreur Code: 400. Mauvaise Requête";
                break;
            }
            case 401: {
                errorMsg = "Erreur Code: 401. Interdit";
                break;
            }
            case 404: {
                errorMsg = "Erreur Code: 404. Pas trouvé";
                break;
            }
            case 500: {
                errorMsg = "Erreur Code: 500. Erreur serveur interne";
                break;
            }
            default: {
            	errorMsg = "Erreur inconnue";
            	break;
            }
        }
        
        model.addAttribute("errorMsg", errorMsg);
        model.addAttribute("errorCode", httpErrorCode);
        return "pageErreur";
    }
     
    private int getErrorCode(HttpServletRequest httpRequest) {
        return (Integer) httpRequest
          .getAttribute("javax.servlet.error.status_code");
    }

	@Override
	public String getErrorPath() {

		return "/error";
	}
	
}
