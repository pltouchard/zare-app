package com.projetZare.auth.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.projetZare.auth.service.api.ApiCallsService;
import com.projetZare.auth.service.impl.SecurityServiceImpl;

@Controller
public class HomeController {
	
	
	@Autowired
	SecurityServiceImpl securityServiceImpl;
	
	@Autowired
	ApiCallsService apiCallsService;

	
	@GetMapping({"/", "/welcome"})
    public String welcome(Model model, HttpSession httpSession) {
	 	Object token = httpSession.getAttribute("jwtToken");
		 try {
			 if(token == null) {
				 String getToken = securityServiceImpl.call_me();
				 
				 httpSession.setAttribute("jwtToken", getToken);
			 }
			 
			 		
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return "redirect:logout?connection";
	    }
		 
        return "redirect:home";
    }
	
	@GetMapping({"/home"})
	public String home(Model model, HttpSession httpSession) {
		Object token = httpSession.getAttribute("jwtToken");
		
		model.addAttribute("reservations", apiCallsService.getAllReservationsByDate(null, httpSession));
		model.addAttribute("nomPage", "Tableau de bord");
		if(token != null) {
			model.addAttribute("userToken", token.toString());
		}else {
			return "redirect:logout";
		}
		
       
		return "redirect:reservation";
	}
	
	@GetMapping({"/tableau-bord"})
	public String tdb(Model model, HttpSession httpSession) {
		Object token = httpSession.getAttribute("jwtToken");
		
		model.addAttribute("reservations", apiCallsService.getAllReservationsByDate(null, httpSession));
		model.addAttribute("nomPage", "Tableau de bord");
		if(token != null) {
			model.addAttribute("userToken", token.toString());
		}else {
			return "redirect:logout";
		}
		
		return "home";
	}
}
