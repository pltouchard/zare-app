package com.projetZare.auth.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projetZare.auth.model.Reservation;
import com.projetZare.auth.service.api.ApiCallsService;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

@Controller
public class RapportsController {
	
	@Autowired
	private ApiCallsService apiCallsService;
	
	private JasperReport rapportJson;
	private JasperReport rapportActiviteJson;

	@GetMapping({"/rapports"})
	public String reservation(Model model, HttpSession httpSession) {
		Object token = httpSession.getAttribute("jwtToken");
		
		model.addAttribute("nomPage", "Rapports");
		model.addAttribute("userToken", token);
		return "rapports";
		
	}
	
	@PostConstruct
	  public void compilerRapportJson() throws Exception {
	    InputStream modeleInputStream = this.getClass().getResourceAsStream("/reservationsJson.jrxml");
	    rapportJson = JasperCompileManager.compileReport(modeleInputStream);
	  }
	
	@GetMapping(path="/generation-rapport-json-jour.xlsx")
	  public void produireRapportJson(OutputStream out, HttpServletResponse response, HttpSession httpSession, String dateResa) throws Exception {

		List<Reservation> reservations = new ArrayList<Reservation>();
		reservations = apiCallsService.getAllReservationsByDate(dateResa, httpSession);
		
		File responseResaJour = ResourceUtils.getFile("classpath:/responseResasJour.json");

		ObjectMapper mapper = new ObjectMapper();
		 mapper.writeValue(responseResaJour, reservations);
		
		InputStream initialStream = new FileInputStream(responseResaJour);
		byte[] buffer = new byte[initialStream.available()];
		initialStream.read(buffer);
			 
		File targetFile = ResourceUtils.getFile("classpath:/resasJour.tmp");
		OutputStream outStream = new FileOutputStream(targetFile);
		outStream.write(buffer);
		ByteArrayInputStream jsonDataStream = new ByteArrayInputStream(buffer);
		
		JsonDataSource ds = new JsonDataSource(jsonDataStream);
	    //Create HashMap to add report parameters
		Map<String, Object> parameters = new HashMap<>();
	    parameters.put("dateSelected", dateResa);
	    //Create Jasper Print object passing report, parameter json data source.
	    JasperPrint jasperPrint = JasperFillManager.fillReport(rapportJson, parameters, ds);
	    
	    JRXlsxExporter exporter = new JRXlsxExporter();
        SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
        reportConfigXLS.setSheetNames(new String[] { "Rapport du jour" });
        exporter.setConfiguration(reportConfigXLS);
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
        response.setHeader("Content-Disposition", "attachment;filename=RapportReservations_" + dateResa + ".xlsx");
        response.setContentType("application/octet-stream");
        exporter.exportReport();
        
        initialStream.close();
        outStream.close();
	}
	
	
	@PostConstruct
	  public void compilerRapportActiviteJson() throws Exception {
	    InputStream modeleInputStream = this.getClass().getResourceAsStream("/rapportActivitePlageJson.jrxml");
	    rapportActiviteJson = JasperCompileManager.compileReport(modeleInputStream);
	  }
	
	@GetMapping(path="/generation-rapport-json-activite.xlsx")
	  public void produireRapportActiviteJson(OutputStream out, HttpServletResponse response, HttpSession httpSession, String dateDebut, String dateFin) throws Exception {
		
		List<Reservation> reservations = new ArrayList<Reservation>();
		
		reservations = apiCallsService.getAllReservationByPlageDates(dateDebut, dateFin, httpSession);

		File responseActivite = ResourceUtils.getFile("classpath:/responseActivite.json");
		
		ObjectMapper mapper = new ObjectMapper();
		 mapper.writeValue(responseActivite, reservations);
		
		InputStream initialStream = new FileInputStream(responseActivite);
		byte[] buffer = new byte[initialStream.available()];
		initialStream.read(buffer);
		
		File targetFile = ResourceUtils.getFile("classpath:/activiteDates.tmp");
		
		OutputStream outStream = new FileOutputStream(targetFile);
		outStream.write(buffer);
		ByteArrayInputStream jsonDataStream = new ByteArrayInputStream(buffer);
		
		JsonDataSource ds = new JsonDataSource(jsonDataStream);
	    //Create HashMap to add report parameters
		Map<String, Object> parameters = new HashMap<>();
	    //Add title parameter. Make sure the key is same name as what you named the parameters in jasper report.
	    parameters.put("date_debut", dateDebut);
	    parameters.put("date_fin", dateFin);
	    //Create Jasper Print object passing report, parameter json data source.
	    JasperPrint jasperPrint = JasperFillManager.fillReport(rapportActiviteJson, parameters, ds);
	    
	    JRXlsxExporter exporter = new JRXlsxExporter();
      SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
      reportConfigXLS.setSheetNames(new String[] { "Rapport du jour" });
      exporter.setConfiguration(reportConfigXLS);
      exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
      exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
      response.setHeader("Content-Disposition", "attachment;filename=RapportActivite_" + dateDebut + "-" + dateFin + ".xlsx");
      response.setContentType("application/octet-stream");
      exporter.exportReport();
      
      initialStream.close();
      outStream.close();
	}
}
