package com.projetZare.auth.controllers;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
    
    @Autowired
    private MessageSource messageSource;

    @GetMapping("/login")
    public String login(Model model, String error, String logout, String deconnection) {
        if (error != null)
            model.addAttribute("error", messageSource.getMessage("Logout.message.erreur",null, Locale.FRANCE) );

        if (logout != null)
            model.addAttribute("message", messageSource.getMessage("Logout.message.deconnection",null, Locale.FRANCE));
        
        if (deconnection != null)
            model.addAttribute("message", messageSource.getMessage("Logout.message.inactivite",null, Locale.FRANCE));

        return "login";
    }
}
