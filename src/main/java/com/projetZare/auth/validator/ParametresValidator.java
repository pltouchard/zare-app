package com.projetZare.auth.validator;

import java.sql.Time;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.projetZare.auth.model.ParametresDto;

@Component
public class ParametresValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return ParametresDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ParametresDto parametresDto = (ParametresDto) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1MidiDebut", "Parametres.creneau.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1MidiFin", "Parametres.creneau.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1MidiNbResas", "Parametres.CouvertsMax.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1SoirDebut", "Parametres.creneau.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1SoirFin", "Parametres.creneau.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1SoirNbResas", "Parametres.CouvertsMax.empty");

		//test each numeric value if null and set it to 0
		if(parametresDto.getCreneau1MidiNbResas() == null) {
			parametresDto.setCreneau1MidiNbResas(0);
		}
		if(parametresDto.getCreneau2MidiNbResas() == null) {
			parametresDto.setCreneau2MidiNbResas(0);
		}
		if(parametresDto.getCreneau3MidiNbResas() == null) {
			parametresDto.setCreneau3MidiNbResas(0);
		}
		if(parametresDto.getCreneau1SoirNbResas() == null) {
			parametresDto.setCreneau1SoirNbResas(0);
		}
		if(parametresDto.getCreneau2SoirNbResas() == null) {
			parametresDto.setCreneau2SoirNbResas(0);
		}
		if(parametresDto.getCreneau3SoirNbResas() == null) {
			parametresDto.setCreneau3SoirNbResas(0);
		}
		
		//Créneau check : for each 3 créneau midi and soir : if not emtpy, if nber of reservations is set and if start time >= end time previous creneau 
		if(parametresDto.getCreneau1MidiDebut() != "" && parametresDto.getCreneau1MidiFin() != "") {
			if(!checkCreneauDebutFin(parametresDto.getCreneau1MidiDebut(), parametresDto.getCreneau1MidiFin())) {
				errors.rejectValue("creneau1MidiDebut", "Parametres.creneau.errorStartEnd");
			}
		}else {
			errors.rejectValue("creneau1MidiDebut", "Parametres.creneau.errorStartEnd");
			errors.rejectValue("creneau1MidiFin", "Parametres.creneau.errorStartEnd");
		}
		
		if(parametresDto.getCreneau2MidiDebut() != "" && parametresDto.getCreneau2MidiFin() != "" && parametresDto.getCreneau1MidiFin() != "") {
			if(!checkCreneauDebutFin(parametresDto.getCreneau2MidiDebut(), parametresDto.getCreneau2MidiFin())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametresDto.getCreneau2MidiNbResas() <= 0) {
				errors.rejectValue("creneau2MidiNbResas", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametresDto.getCreneau1MidiFin(), parametresDto.getCreneau2MidiDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametresDto.getCreneau2MidiDebut() != "" && !(parametresDto.getCreneau2MidiFin() != "") || !(parametresDto.getCreneau2MidiDebut() != "") && parametresDto.getCreneau2MidiFin() != "") {
				errors.rejectValue("creneau2MidiNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		if(parametresDto.getCreneau3MidiDebut() != "" && parametresDto.getCreneau3MidiFin() != "" && parametresDto.getCreneau2MidiFin() != "") {
			if(!checkCreneauDebutFin(parametresDto.getCreneau3MidiDebut(), parametresDto.getCreneau3MidiFin())) {
				errors.rejectValue("creneau3MidiDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametresDto.getCreneau3MidiNbResas() <= 0) {
				errors.rejectValue("creneau3MidiNbResas", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametresDto.getCreneau2MidiFin(), parametresDto.getCreneau3MidiDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametresDto.getCreneau3MidiDebut() != "" && !(parametresDto.getCreneau3MidiFin() != "") || !(parametresDto.getCreneau3MidiDebut() != "") && parametresDto.getCreneau3MidiFin() != "") {
				errors.rejectValue("creneau3MidiNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		if(parametresDto.getCreneau1SoirDebut() != "" && parametresDto.getCreneau1SoirFin() != "") {
			if(!checkCreneauDebutFin(parametresDto.getCreneau1SoirDebut(), parametresDto.getCreneau1SoirFin())) {
				errors.rejectValue("creneau1SoirDebut", "Parametres.creneau.errorStartEnd");
			}
		}else {
			errors.rejectValue("creneau1SoirDebut", "Parametres.creneau.errorStartEnd");
			errors.rejectValue("creneau1SoirFin", "Parametres.creneau.errorStartEnd");
		}
		
		if(parametresDto.getCreneau2SoirDebut() != "" && parametresDto.getCreneau2SoirFin() != "" && parametresDto.getCreneau1SoirFin() != "") {
			if(!checkCreneauDebutFin(parametresDto.getCreneau2SoirDebut(), parametresDto.getCreneau2SoirFin())) {
				errors.rejectValue("creneau2SoirDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametresDto.getCreneau2SoirNbResas() <= 0) {
				errors.rejectValue("creneau2SoirDebut", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametresDto.getCreneau1SoirFin(), parametresDto.getCreneau2SoirDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametresDto.getCreneau2SoirDebut() != "" && !(parametresDto.getCreneau2SoirFin() != "") || !(parametresDto.getCreneau2SoirDebut() != "") && parametresDto.getCreneau2SoirFin() != "") {
				errors.rejectValue("creneau2SoirNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		if(parametresDto.getCreneau3SoirDebut() != "" && parametresDto.getCreneau3SoirFin() != "" &parametresDto.getCreneau2SoirFin() != "") {
			if(!checkCreneauDebutFin(parametresDto.getCreneau3SoirDebut(), parametresDto.getCreneau3SoirFin())) {
				errors.rejectValue("creneau3SoirDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametresDto.getCreneau3SoirNbResas() <= 0) {
				errors.rejectValue("creneau3SoirNbResas", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametresDto.getCreneau2SoirFin(), parametresDto.getCreneau3SoirDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametresDto.getCreneau3SoirDebut() != "" && !(parametresDto.getCreneau3SoirFin() != "") || !(parametresDto.getCreneau3SoirDebut() != "") && parametresDto.getCreneau3SoirFin() != "") {
				errors.rejectValue("creneau3SoirNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		
	}
	
	
	
	public boolean checkCreneauDebutFin(String tDebut, String tFin) {
		Time creneauDebut = Time.valueOf(tDebut);
		Time creneauFin = Time.valueOf(tFin);
		if(creneauDebut.compareTo(creneauFin) > 0) {
			return false;
		}
		return true;
	}

}
