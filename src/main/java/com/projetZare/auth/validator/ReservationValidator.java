package com.projetZare.auth.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.projetZare.auth.model.Reservation;
import com.projetZare.auth.model.ReservationDto;

@Component
public class ReservationValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		return ReservationDto.class.equals(aClass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ReservationDto reservation = (ReservationDto) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nomResa", "NotEmpty");
		if (reservation.getNomResa().length() > 0 && (reservation.getNomResa().length() < 3 || reservation.getNomResa().length()  > 100)) {
            errors.rejectValue("nomResa", "Taille.reservation.nom");
        }
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telephoneResa", "NotEmpty");
		if (reservation.getTelephoneResa().length() < 10 || reservation.getTelephoneResa().length()  > 15) {
            errors.rejectValue("telephoneResa", "Taille.reservation.telephone");
        }
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailResa", "NotEmpty");
		if (reservation.getMailResa().length() < 6 || reservation.getMailResa().length()  > 80) {
            errors.rejectValue("mailResa", "Taille.reservation.mail");
        }
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nbCouvertsResa", "NotEmpty");
		if (reservation.getNbCouvertsResa() < 1 || reservation.getNbCouvertsResa()  > 15) {
            errors.rejectValue("nbCouvertsResa", "Taille.reservation.nombreCouverts");
        }
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneauResaId", "NotEmpty");
		if(reservation.getCreneauResaId() == 0) {
			errors.rejectValue("creneauResaId", "NotEmpty");
		}
		
		
		
	}


}
