package com.projetZare.auth.validator;

import java.sql.Time;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.projetZare.auth.model.ParametreParticulierDto;

@Component
public class ParametreParticulierValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return ParametreParticulierDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ParametreParticulierDto parametreParticulierDto = (ParametreParticulierDto) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dates", "Parametres.dates.empty");
		if(parametreParticulierDto.getOuvertMidiP() == true) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1MidiDebut", "Parametres.creneau.empty");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1MidiFin", "Parametres.creneau.empty");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1MidiNbResas", "Parametres.CouvertsMax.empty");
		}
		if(parametreParticulierDto.getOuvertSoirP() == true) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1SoirDebut", "Parametres.creneau.empty");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1SoirFin", "Parametres.creneau.empty");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "creneau1SoirNbResas", "Parametres.CouvertsMax.empty");
		}
		
		
		//test each numeric value if null and set it to 0
		if(parametreParticulierDto.getCreneau1MidiNbResas() == null) {
			parametreParticulierDto.setCreneau1MidiNbResas(0);
		}
		if(parametreParticulierDto.getCreneau2MidiNbResas() == null) {
			parametreParticulierDto.setCreneau2MidiNbResas(0);
		}
		if(parametreParticulierDto.getCreneau3MidiNbResas() == null) {
			parametreParticulierDto.setCreneau3MidiNbResas(0);
		}
		if(parametreParticulierDto.getCreneau1SoirNbResas() == null) {
			parametreParticulierDto.setCreneau1SoirNbResas(0);
		}
		if(parametreParticulierDto.getCreneau2SoirNbResas() == null) {
			parametreParticulierDto.setCreneau2SoirNbResas(0);
		}
		if(parametreParticulierDto.getCreneau3SoirNbResas() == null) {
			parametreParticulierDto.setCreneau3SoirNbResas(0);
		}
		
		//Créneau check : for each 3 créneau midi and soir : if not emtpy, if nber of reservations is set and if start time > end time
		if(parametreParticulierDto.getOuvertMidiP() == true) {
			if(parametreParticulierDto.getCreneau1MidiDebut() != "" && parametreParticulierDto.getCreneau1MidiFin() != "") {
				if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau1MidiDebut(), parametreParticulierDto.getCreneau1MidiFin())) {
					errors.rejectValue("creneau1MidiDebut", "Parametres.creneau.errorStartEnd");
				}
			}else {
				errors.rejectValue("creneau1MidiDebut", "Parametres.creneau.errorStartEnd");
				errors.rejectValue("creneau1MidiFin", "Parametres.creneau.errorStartEnd");
			}
		}
		
		
		if(parametreParticulierDto.getCreneau2MidiDebut() != "" && parametreParticulierDto.getCreneau2MidiFin() != "") {
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau2MidiDebut(), parametreParticulierDto.getCreneau2MidiFin())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametreParticulierDto.getCreneau2MidiNbResas() <= 0) {
				errors.rejectValue("creneau2MidiNbResas", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau1MidiFin(), parametreParticulierDto.getCreneau2MidiDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametreParticulierDto.getCreneau2MidiDebut() != "" && !(parametreParticulierDto.getCreneau2MidiFin() != "") || !(parametreParticulierDto.getCreneau2MidiDebut() != "") && parametreParticulierDto.getCreneau2MidiFin() != "") {
				errors.rejectValue("creneau2MidiNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		if(parametreParticulierDto.getCreneau3MidiDebut() != "" && parametreParticulierDto.getCreneau3MidiFin() != "") {
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau3MidiDebut(), parametreParticulierDto.getCreneau3MidiFin())) {
				errors.rejectValue("creneau3MidiDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametreParticulierDto.getCreneau3MidiNbResas() <= 0) {
				errors.rejectValue("creneau3MidiNbResas", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau2MidiFin(), parametreParticulierDto.getCreneau3MidiDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametreParticulierDto.getCreneau3MidiDebut() != "" && !(parametreParticulierDto.getCreneau3MidiFin() != "") || !(parametreParticulierDto.getCreneau3MidiDebut() != "") && parametreParticulierDto.getCreneau3MidiFin() != "") {
				errors.rejectValue("creneau3MidiNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		if(parametreParticulierDto.getOuvertSoirP() == true) {
			if(parametreParticulierDto.getCreneau1SoirDebut() != "" && parametreParticulierDto.getCreneau1SoirFin() != "") {
				if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau1SoirDebut(), parametreParticulierDto.getCreneau1SoirFin())) {
					errors.rejectValue("creneau1SoirDebut", "Parametres.creneau.errorStartEnd");
				}
			}else {
				errors.rejectValue("creneau1SoirDebut", "Parametres.creneau.errorStartEnd");
				errors.rejectValue("creneau1SoirFin", "Parametres.creneau.errorStartEnd");
			}
		}
		
		if(parametreParticulierDto.getCreneau2SoirDebut() != "" && parametreParticulierDto.getCreneau2SoirFin() != "") {
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau2SoirDebut(), parametreParticulierDto.getCreneau2SoirFin())) {
				errors.rejectValue("creneau2SoirDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametreParticulierDto.getCreneau2SoirNbResas() <= 0) {
				errors.rejectValue("creneau2SoirDebut", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau1SoirFin(), parametreParticulierDto.getCreneau2SoirDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametreParticulierDto.getCreneau2SoirDebut() != "" && !(parametreParticulierDto.getCreneau2SoirFin() != "") || !(parametreParticulierDto.getCreneau2SoirDebut() != "") && parametreParticulierDto.getCreneau2SoirFin() != "") {
				errors.rejectValue("creneau2SoirNbResas", "Parametres.creneauNbResas.errorMissing");
		}
		
		if(parametreParticulierDto.getCreneau3SoirDebut() != "" && parametreParticulierDto.getCreneau3SoirFin() != "") {
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau3SoirDebut(), parametreParticulierDto.getCreneau3SoirFin())) {
				errors.rejectValue("creneau3SoirDebut", "Parametres.creneau.errorStartEnd");
			}
			if(parametreParticulierDto.getCreneau3SoirNbResas() <= 0) {
				errors.rejectValue("creneau3SoirNbResas", "Parametres.creneauNbResas.errorNbResasNotSet");
			}
			if(!checkCreneauDebutFin(parametreParticulierDto.getCreneau2SoirFin(), parametreParticulierDto.getCreneau3SoirDebut())) {
				errors.rejectValue("creneau2MidiDebut", "Parametres.creneau.errorCoherenceCreneaux");
			}
		}else if(parametreParticulierDto.getCreneau3SoirDebut() != "" && !(parametreParticulierDto.getCreneau3SoirFin() != "") || !(parametreParticulierDto.getCreneau3SoirDebut() != "") && parametreParticulierDto.getCreneau3SoirFin() != "") {
				errors.rejectValue("creneau3SoirNbResas", "Parametres.creneauNbResas.errorMissing");
		}
	}
	
	public boolean checkCreneauDebutFin(String tDebut, String tFin) {
		Time creneauDebut = Time.valueOf(tDebut);
		Time creneauFin = Time.valueOf(tFin);
		if(creneauDebut.compareTo(creneauFin) > 0) {
			return false;
		}
		return true;
	}

}
